DELETE FROM Osoba WHERE idOsoba IS NOT NULL;
DELETE FROM Korisnik WHERE idKorisnik IS NOT NULL;

ALTER SEQUENCE Osoba_idOsoba_seq RESTART WITH 1;
ALTER SEQUENCE Korisnik_idKorisnik_seq RESTART WITH 1;

INSERT INTO Osoba (imeOsoba, prezimeOsoba, datumRodenjaOsoba, oibOsoba, emailOsoba, lozinkaOsoba) VALUES
('Mario','Perić','01/01/1994','41521583425','mario.peric@gmail.com','-'),
('Josip','Antolić','25/08/1989','51522125829','josip.antolic@gmail.com','-'),
('Antonio','Vujčić','13/04/1995','28123131292','antonio.vujcic@fitness.hr','-');

INSERT INTO Korisnik (visinaKorisnik,tezinaKorisnik,idOsoba) VALUES
('178','75',1),
('181','96',2),
('166','54',3);


INSERT INTO Intenzitet (nazivIntenzitet, opisIntenzitet) VALUES
('Niski intenzitet', 'Broj otkucaja srca ne prelazi 40-60% maksimalnog broja otkucaja. Velike pauze između vježbi. Prigodno za početnike.'),
('Srednji intenzitet', 'Broj otkucaja srca na oko 70% maksimalnog broja otkucaja. Pauze srednje duljine između izvođenja vježbi. Prigodno za utrenirane vježbače.'),
('Visoki intenzitet', 'Broj otkucaja na oko 90% od maksimalnog. Pauze između serija su vrlo kratke. Prigodno za sportaše na profesionalnoj razini.');

INSERT INTO Lokacija(nazivLokacija, vanjskaLokacija) VALUES
('BITFIT Ravnice', 0),
('BITFIT Dubrava', 0),
('Park Maksimir', 1);

INSERT INTO Vrsta (nazivVrsta, opisVrsta) VALUES
('Cardio', 'Razvoj, održavanje i poboljšanje sposobnosti koje nam omogućavaju dugotrajno provođenje određene aktivnosti definiranim intenzitetom prije pojave umora.'),
('Trening snage', 'Tip treninga namijenjen za povećanje mišićne mase i podizanje fizičke spreme.'),
('Funkcionalni trening', 'Koncept treninga kojim se utječe na istovremeni razvoj što većeg broja motoričkih i funkcionalnih sposobnosti i osobina te na sastav tijela.'),
('Intervalni trening', 'Intervalni trening izvodi se izmjenično u periodima rada i odmora. Kombiniraju se vježbe snage i izdržljivosti s ili bez rekvizita.');


INSERT INTO Osoba (imeOsoba, prezimeOsoba, datumRodenjaOsoba, oibOsoba, emailOsoba, lozinkaOsoba) VALUES
('Tomislav','Josipović','15/11/1993','82842182731','tomislav.josipovic@fitness.hr','-'),
('Mario','Andrić','23/05/1986','58582182731','mario.andric@treninzi.hr','-');

INSERT INTO Voditelj (zavrsenKif, idOsoba) VALUES
(0,4),
(1,5);

INSERT INTO Clanarina (idKorisnik, vrijemePlacanjaClanarina, vrijemeIstekaClanarina, cijenaClanarina, propusteniDolasci) VALUES
(15,now(),now()+'1 month'::INTERVAL,'170','0');

