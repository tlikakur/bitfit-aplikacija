import 'package:flutter/material.dart';
import 'styles.dart';
import 'locations.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';


class TermActivity extends StatefulWidget {
  final int locationId;
	TermActivity({Key key, @required this.locationId}) : super(key: key);

	@override
	_TermActivityState createState() => _TermActivityState();
}

class _TermActivityState extends State<TermActivity> {

     
     Map<String, dynamic> responseData;

  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

	var done = false;
  var prefs;
  

  Future<void> getTermsOnLocation() async{

    prefs = await _getSharedPrefs();
    print('/mobile/termsOnLocation?locationid=' + widget.locationId.toString());
    var response =  await http.get(
      Uri.parse(URI + '/mobile/termsOnLocation?locationid=' + widget.locationId.toString()),
      headers: defaultHeader,  
    );

    if(response.statusCode == 200)
      responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
  }

  List<dynamic> terms;

  @mustCallSuper
  void initState(){
    (() async {
      await getTermsOnLocation();
      print("RESPONSE DATA: " + responseData.toString());
      setState(() {

        terms = responseData['terms'];
        done = true;
        print("Terms: " + terms.toString());
      });
    })();
    super.initState();
  }
	
	@override
	Widget build(BuildContext context) {

		final Size screen = MediaQuery.of(context).size;

		return Scaffold(
			resizeToAvoidBottomInset: false,
			body: Container(
							
							constraints: BoxConstraints.expand(),
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("res/img/workout-bg.png"),
									fit: BoxFit.cover
						)),
			
              child: SingleChildScrollView(
							child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                
								children: [
                
                  // Spacer od vrha do linka za povratak na lokacije
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za povratak na lokacije
                  link(context, "LOKACIJE", "left", LocationActivity()),
         

                  // Spacer od teksta "Lokacije" do teksta "ODABIR TERMINA"
                  SizedBox(height: screen.height * 0.017),

                  // Naslov "TERMINI - Ime Lokacije"
                  if(prefs != null && prefs.getString('type') == "user" && responseData['success'] == 1) heading(context, "TERMINI - BITFIT"),


                  // Expansion Card sa informacijama o terminu
                  if(terms != null) for(Map<String, dynamic> term in terms)
                  termCard(context, term['idtermin'].toString(), term['datumtermin'] + " " + term['vrijemepocetak'] + 'h - ' + term['vrijemezavrsetak'] + 'h', term['nazivvrsta'], "Nema preporuka", term['imeosoba'] + " " + term['prezimeosoba'], term['brojclanova'].toString() + "/" + term['maxbrojclanova'].toString(), term['idintenzitet'], "REZERVIRAJ", null, term['restrictions']),
          
                  // If loading is done and there are no available terms
                  if(done && (terms == null  || terms.length == 0))
                    // TO DO: Move no-items-message to Styles
                    Column(
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/5),
                        Image.asset('res/icons/empty-icon.png',scale:6),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                        Text("TRENUTNO NEMA DOSTUPNIH TERMINA :C",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 16)),
                        SizedBox(height: MediaQuery.of(context).size.height/75),
                        Text("Molimo odaberite neku drugu lokaciju.",
                        style: TextStyle(color:Colors.white, fontSize: 13)),



            ],

          ),

          ])))
		);
	}
}