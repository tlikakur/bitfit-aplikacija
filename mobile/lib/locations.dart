import 'package:flutter/material.dart';
import 'styles.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';
import 'profile.dart';


class LocationActivity extends StatefulWidget {
	LocationActivity({Key key}) : super(key: key);

	@override
	_LocationActivityState createState() => _LocationActivityState();
}

class _LocationActivityState extends State<LocationActivity> {

   Map<String, dynamic> responseData;

  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

	
  
  var prefs;

  Future<void> getProfileData() async{

    prefs = await _getSharedPrefs();

    var response =  await http.get(
      Uri.parse(URI + '/mobile/locations'),
      headers: defaultHeader
    );

    if(response.statusCode == 200)
      responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
  }

  List<dynamic> locations;

  @mustCallSuper
  void initState(){
    (() async {
      await getProfileData();
      //print("RESPONSE DATA: " + responseData.toString());
      setState(() {
        locations = responseData['locations'];
        print("Locations: " + locations.toString());
      });
    })();
    super.initState();
  }
	
	@override
	Widget build(BuildContext context) {
		final Size screen = MediaQuery.of(context).size;

		return Scaffold(

      // Sprjecava overflow kad se otvara keyboard
			resizeToAvoidBottomInset: false,

			body: Stack(
        children:[ 
          Container(
			  				
			  		constraints: BoxConstraints.expand(),
			  		decoration: BoxDecoration(
			  			image: DecorationImage(
			  				image: AssetImage("res/img/workout-bg.png"),
			  				fit: BoxFit.cover
			  		)),
			  
			  		child: SingleChildScrollView(
              child: Column(
			  			  children: [

                  // Spacer od vrha do linka za povratak na lokacije
                    SizedBox(height: screen.height * 0.06),

                    // Tekst (tj. link) za povratak na lokacije
                    link(context, "VAŠ PROFIL", "left", ProfileActivity()),

			  				  	// Spacer od vrha do teksta "ODABIR LOKACIJE"
			  				  	SizedBox(height: screen.height * 0.017),

                    // Naslov "ODABIR LOKACIJE"
			  				  	if(prefs != null && prefs.getString('type') == "coach") heading(context, "NOVI TERMIN - ODABIR LOKACIJE"),
                    if(prefs != null && prefs.getString('type') == "user") heading(context, "ODABIR LOKACIJE"),

                    if(locations != null && prefs.getString('type') != null) for(Map<String, dynamic> location in locations)
                    
			  				  	  location['vanjskalokacija'] == 0 ? 

                      // Lokacija unutarnjeg tipa
                      locationInside(context, prefs.getString('type'), location['nazivlokacija'], location['adresalokacija'] + ", " + location['pbmjesto'].toString() + " " + location['nazivmjesto'], location['idlokacija']) :
                      
                      // Lokacija vanjskog tipa
			  				  	  locationOutside(context, prefs.getString('type'), location['nazivlokacija'], location['adresalokacija'] + ", " + location['pbmjesto'].toString() + " " + location['nazivmjesto'], location['idlokacija'])                    
                  ]
                ))
			        )
            ]
          )
		);
	}
}