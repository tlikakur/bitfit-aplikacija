import 'package:flutter/material.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'term.dart';
import 'chooseTrainingDetails.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'extras.dart';
import 'package:http/http.dart' as http;
import 'profile.dart';
import 'userList.dart';

dynamic _getSharedPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs;
}

var prefs;
var visible = true;

Map<String, dynamic> responseData;

// POST for term reservation
Future<http.Response> reserveTerm(String id) async{

  prefs = await _getSharedPrefs();
  print(URI + '/mobile/reserveTerm');
  return http.post(
    Uri.parse(URI + '/mobile/reserveTerm'),
    headers: defaultHeader, 
    body: jsonEncode(<String, dynamic>{
      'termid': id,
      'type': prefs.getString('type'),
      'id': prefs.getInt('id')
    }),
  );
}



// POST for term cancellation
Future<http.Response> cancelTerm(String id) async{

  prefs = await _getSharedPrefs();
  print(URI + '/mobile/cancelTerm');
  return http.post(
    Uri.parse(URI + '/mobile/cancelTerm'),
    headers: defaultHeader, 
    body: jsonEncode(<String, dynamic>{
      'termid': id,
      'type': prefs.getString('type'),
      'id': prefs.getInt('id')
    }),
  );
}

// POST for term cancellation
Future<http.Response> setArrival(int userid, int termid, int status) async{

  
  print(URI + '/mobile/setArrival');
  return http.post(
    Uri.parse(URI + '/mobile/setArrival'),
    headers: defaultHeader, 
    body: jsonEncode(<String, dynamic>{
      'termid': termid,
      'userid': userid,
      'status': status
    }),
  );
}

dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

final basicTextField = new TextStyle(color: Colors.white, fontFamily: "Product-Sans");
final linkBackStyle = new TextStyle(fontSize: 12, fontFamily: "Product-Sans", color: Colors.white,fontWeight: FontWeight.bold);
final termDetailStyle = new TextStyle(fontFamily: "Product-Sans", color: Color.fromRGBO(75,75,75,1.0), fontSize:12,fontWeight: FontWeight.w500);
final headingStyle = new TextStyle(fontFamily: "Product-Sans", fontSize: 17,color: Colors.white,fontWeight: FontWeight.bold);
final locationMainTextStyle  = new TextStyle(fontFamily: "Product-Sans", fontSize:17, color: Color.fromRGBO(0, 62, 165, 1.0));
final termTypeTextStyle  = new TextStyle(fontFamily: "Product-Sans", fontSize:16, color: Color.fromRGBO(0, 55, 145, 1.0), fontWeight: FontWeight.w500);
final locationAdditionalTextStyle = new TextStyle(fontFamily: "Product-Sans",fontSize:13, color: Colors.black);
final termMainTextStyle = new TextStyle(fontFamily: "Product-Sans", color:Colors.black,fontSize: 14, fontWeight: FontWeight.w400);
final headingStyle2 = new TextStyle(fontFamily: "Product-Sans", fontSize: 15,color: Colors.white, fontWeight: FontWeight.w500);
final warningStyle = new TextStyle(fontFamily: "Product-Sans", fontSize: 12, color: Colors.yellow[700], fontWeight: FontWeight.w500);

snackbar(BuildContext context, Color bgColor, String msg){

  return SnackBar(margin: EdgeInsets.only(bottom: 50, left: 30, right: 30),
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: bgColor,
                  content: Text(msg, style: TextStyle(color: Colors.white), textAlign: TextAlign.center),
                  duration: Duration(milliseconds: 1500),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)));
}


locationInside(BuildContext context, String userType, String locationName, String locationDetails, int locationId){

  final Size screen = MediaQuery.of(context).size;

  return GestureDetector(
    onTap: (){

      if(userType == "coach") Navigator.pushReplacement(context, 
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => ChooseTrainingDetailsActivity(locationId: locationId),
            transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 200),
        ));

      if(userType == "user") Navigator.pushReplacement(context, 
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => TermActivity(locationId: locationId),
            transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 200),
        ));
    },
    
    child: Container(
			padding: EdgeInsets.only(left: 20, top: 20),
			margin: EdgeInsets.symmetric(horizontal: screen.width * 0.1, vertical: screen.height * 0.02),
			height: 120,
			width:double.infinity,
			decoration: BoxDecoration(
			  color: Colors.white,
			  borderRadius: BorderRadius.circular(30), //border corner radius
			),

			child:Stack(
        children: [
          // Slika
			  	Positioned(right: 0, bottom: 0,
            child: Opacity(opacity: 0.4,
			  	    child: Image(
                image: AssetImage('res/img/city-art.png'),
                width: 80, height: 130, fit: BoxFit.fitHeight
              )
			  	)),

			  	// Ime lokacije
			  	Positioned(top:20,child:Text(locationName, style: locationMainTextStyle)),

			  	// Opis lokacije
			  	Positioned(top: 40, child:Text(locationDetails, style: locationAdditionalTextStyle)),
			  ]),
		),
	);

}


locationOutside(BuildContext context, String userType, String locationName, String locationDetails, int locationId){

  final Size screen = MediaQuery.of(context).size;

  return GestureDetector(
    onTap: (){
      Navigator.pushReplacement(context, 
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => TermActivity(locationId: locationId),
            transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 200),
        ));
    },
    
    child: Container(
			padding: EdgeInsets.only(left: 20, top: 20),
			margin: EdgeInsets.symmetric(horizontal: screen.width * 0.1, vertical: screen.height * 0.02),
			height: 120,
			width:double.infinity,
			decoration: BoxDecoration(
			  color: Colors.white,
			  borderRadius: BorderRadius.circular(30), //border corner radius
			),

			child:Stack(
        children: [
          // Slika
			  	Positioned(right: 0, bottom: 0,
            child: Opacity(opacity: 0.4,
			  	    child: Image(
                image: AssetImage('res/img/outdoor-art.png'),
                width: 100, height: 100, fit: BoxFit.fitHeight
              )
			  	)),

			  	// Ime lokacije
			  	Positioned(top:20,child:Text(locationName, style: locationMainTextStyle)),

			  	// Opis lokacije
			  	Positioned(top: 40, child:Text(locationDetails, style: locationAdditionalTextStyle)),
			  ]),
		),
	);
}


heading(BuildContext context, String text){

  final Size screen = MediaQuery.of(context).size;

  return Row(children:[
		SizedBox(width: screen.width * 0.12),
		Text(text, style: headingStyle),
	]);
}


link(BuildContext context, String text, String position, StatefulWidget activity){

  final Size screen = MediaQuery.of(context).size;

  return GestureDetector(
    onTap: () {
      Navigator.pushReplacement(
        context, 
          PageRouteBuilder(
              pageBuilder: (c, a1, a2) => activity,
                transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 200),
          ));
    },

    child: Row(

      mainAxisAlignment: position == "left" ? MainAxisAlignment.start : MainAxisAlignment.end,

      children:[
        if(position == "left") SizedBox(width: screen.width * 0.03),
        if(position == "left") Icon(Icons.arrow_back_ios, color: Colors.white, size: 13),
        Text(text, style: linkBackStyle),
          if(position == "right") Icon(Icons.arrow_forward_ios, color: Colors.white, size: 13),
        if(position == "right") SizedBox(width: screen.width * 0.03),
      ]
    )
  );
}

termCard(BuildContext context, String id, String termMainText, String vrstaTreninga, String preporuke, String voditelj, String mjesta, int intensity, String btnText, String lokacija, List<dynamic> restrictions){

  final Size screen = MediaQuery.of(context).size;
  return Container(margin: EdgeInsets.symmetric(vertical: screen.height * 0.02), padding: EdgeInsets.symmetric(horizontal: 30),
  
  child:  ExpansionTileCard(
    borderRadius: BorderRadius.circular(30), 
    baseColor: Colors.white,
    expandedColor: Colors.white,
    title: Text(termMainText, style: termMainTextStyle),
    children: <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Padding(
        padding: const EdgeInsets.only(
         top: 0,
         bottom: 10,
         left: 20,
         right: 20,
        ),

      child: Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: [
        Row(children:[
          Text("Intenzitet: ", style: termDetailStyle),

          if(intensity != null) for(int i=1; i<=intensity; i++)
            Container(width:14,height:14,decoration:BoxDecoration(border:Border.all(color: Colors.grey),color:Color.fromRGBO(0, 57, 152, 1.0), borderRadius: BorderRadius.circular(10)), margin: EdgeInsets.only(right: 4)),
            

          if(intensity != null) for(int i=0; i<(3-intensity); i++)
            Container(width:14,height:14,decoration:BoxDecoration(border:Border.all(color: Colors.grey),color:Colors.transparent, borderRadius: BorderRadius.circular(10)), margin: EdgeInsets.only(right: 4)),
            

        ]),
        
        if(lokacija != null) Text("Lokacija: " + lokacija, style: termDetailStyle),
        Text("Vrsta treninga: " + vrstaTreninga, style: termDetailStyle),
        Text("Preporuke: " + preporuke, style: termDetailStyle),
        if(voditelj != null) Text("Voditelj: " + voditelj, style: termDetailStyle),
        Text("Rezervirano: " + mjesta, style: termDetailStyle),
        if(restrictions != null) SizedBox(height: 10),
        if(restrictions != null)for(dynamic restriction in restrictions)
         Row(children:[
        Icon(Icons.error_outline, color: Colors.yellow[700], size: 16),
        Text(" " + restriction['nazivogranicenje'], style: warningStyle)]),


        SizedBox(height: 20), // Vertikalni spacer
        if(btnText != null)  Center(
          child: MaterialButton(
        
            minWidth: screen.width * 0.38,
            height: screen.height * 0.038,
            color: Color.fromRGBO(0, 57, 152, 1.0),
            disabledColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0)
            ),

             child: new Text(btnText, 
              style: new TextStyle(
                fontFamily: "Product-Sans",
                color: Colors.white,
                fontSize: 11.0,
                fontWeight: FontWeight.bold
              )),
                      
              onPressed: () async {

              if(visible && btnText == "REZERVIRAJ"){
                var response = await reserveTerm(id);

                if(response.statusCode == 200){

                
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                print("RESPONSE DATA: " + responseData.toString());

                var msg = "";
                var bgColor = Color.fromRGBO(255,255,255,0.0);

                // If login is successful
                if(responseData['success'] == 0){
                   msg = "Greška: ";
                   bgColor = Color.fromRGBO(222, 47, 47, 0.7);
                }
                else if(responseData['success'] == 1)
                   bgColor = Color.fromRGBO(55, 95, 163, 0.7);
                  
                  
                dynamic snackBar = snackbar(context, bgColor, msg + responseData['msg']);

                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                Navigator.pushReplacement(context, 
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => ProfileActivity(),
                  transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 200),
                ));
              }
              }
              else if(visible && btnText == "OTKAŽI"){
                var response = await cancelTerm(id);

                if(response.statusCode == 200){

                
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                print("RESPONSE DATA: " + responseData.toString());

                var msg = "";
                var bgColor = Color.fromRGBO(255,255,255,0.0);

                // If term cancel is not successful
                if(responseData['success'] == 0){
                   msg = "Greška: ";
                   bgColor = Color.fromRGBO(222, 47, 47, 0.7);
                }

                // If term cancel is successful
                else if(responseData['success'] == 1)
                   bgColor = Color.fromRGBO(55, 95, 163, 0.7);
                  
                dynamic snackBar = snackbar(context, bgColor, msg + responseData['msg']);

                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                Navigator.pushReplacement(context, 
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => ProfileActivity(),
                  transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 200),
        ));


              }
            }

          else if(visible && btnText == "POPIS KORISNIKA"){
            Navigator.pushReplacement(context, 
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => UsersListActivity(termid: int.parse(id)),
                  transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 200),
        ));


          }

              
          
              })
      )],
  )),
  )],
));	
}




userCard(BuildContext context, String id, String termid, String imePrezime, String email, String oib, String datumrodenja,  String statusDolaska){

  final Size screen = MediaQuery.of(context).size;

 return GestureDetector(
            onTap: () async {
              print("Desi");
              var status = 0;
              if(statusDolaska == "0") status = 1;
              var response = await setArrival(int.parse(id), int.parse(termid), status);
              if(response.statusCode == 200){
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
            
                if(responseData['success'] == 1){
                  if(statusDolaska == "0") statusDolaska = "1";
                  else if(statusDolaska == "1") statusDolaska = "0";
                  
                }

                

              }

            },

            child: Container(
			padding: EdgeInsets.only(left: 20, top: 10),
			margin: EdgeInsets.only(left: screen.width * 0.08, right: screen.width * 0.08, top: screen.height * 0.02, bottom: screen.height * 0.02),
			height: 80,
			width:double.infinity,
			decoration: BoxDecoration(
			  color: Colors.white,
			  borderRadius: BorderRadius.circular(25), //border corner radius
			),

			child:Stack(
        children: [
          // Slika
			  	Positioned(right: screen.width * 0.25, bottom: 0,
            child: Opacity(opacity: 0.4,
			  	    child: Image(
                image: AssetImage('res/img/stats-art.png'),
                width: 80, height: 80, fit: BoxFit.fitHeight
              )
			  	)),

			  	// Ime lokacije
			  	Positioned(top:10,child:Text(imePrezime, style: locationMainTextStyle)),

			  	// Opis lokacije
			  	Positioned(top: 30, child:Text(email, style: locationAdditionalTextStyle)),

          Positioned(top: 12, right: 25, child: SizedBox(width: 35, height:35, child: Container(
            
          child: SizedBox(
    width: 20,
    child: IconButton(
    icon: statusDolaska == "1" ? Icon(Icons.check, color: Colors.blue[900]) : Icon(Icons.remove_circle_outline, color: Colors.blue[900]),
    iconSize: 25,    
    onPressed: null,
   ),
  ),
     
        
)))
			  ]),
		));
}


heading2(BuildContext context, String text){

  final Size screen = MediaQuery.of(context).size;
  return Container(
    margin: EdgeInsets.only(top: screen.height * 0.03),
    padding: EdgeInsets.symmetric(horizontal: 50),
    child: Row(
      mainAxisAlignment:  MainAxisAlignment.start, 
      children:[Text(text, style: headingStyle2)]),
  );
}



