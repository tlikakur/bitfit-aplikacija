import 'package:flutter/material.dart';
import 'styles.dart';
import 'locations.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';
import 'profile.dart';




class ChooseTrainingDetailsActivity extends StatefulWidget {
  final int locationId;
	ChooseTrainingDetailsActivity({Key key, @required this.locationId}) : super(key: key);

	@override
	_TrainingDetailsActivityState createState() => _TrainingDetailsActivityState();
}

class _TrainingDetailsActivityState extends State<ChooseTrainingDetailsActivity> {


  Map<String, dynamic> types, intensities;

  Future<http.Response> createTerm(int locationid, String termstart, String termend, DateTime termdate, int intensity, int type, int maxusers) async{

    var prefs = await _getSharedPrefs();
    var id = prefs.getInt('id');

    return http.post(
      Uri.parse(URI+'/mobile/createTerm'),
      headers: defaultHeader,
      body: jsonEncode(<String, dynamic>{
        'coachid': id,
        'locationid': locationid,
        'termstart': termstart,
        'termend': termend,
        'termdate': termdate.toString(),
        'intensity': intensity,
        'type': type,
        'maxusers': maxusers
      }),    
    );
  }


  _selectDate(BuildContext context) async {
  final DateTime picked = await showDatePicker(
    context: context,
    initialDate: selectedDate, // Refer step 1
    firstDate: DateTime(2021),
    lastDate: DateTime(2022),
    builder: (BuildContext context, Widget child) {
    return Theme(
      data: ThemeData.light().copyWith(
          primaryColor: const Color(0xFF0906A7),
          accentColor: const Color(0xFF0906A7),
          colorScheme: ColorScheme.light(primary: const Color(0xFF0906A7)),
          buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary
          ),
      ),
      child: child,
    );
  },
  );
  if (picked != null && picked != selectedDate)
    setState(() {
      selectedDate = picked;
    });
}
  
  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);


  var prefs;

  Future<void> dropdownPrepareTypes() async{

  if(types != null && dropdownTrainingTypes.length == 0) for(dynamic mapa in types['types']){
    DropdownMenuItem novi = new DropdownMenuItem(
      child: Center(
        child: Text("   " + mapa['nazivvrsta'].toString(),
          style: new TextStyle(
            fontSize: 14.0,
            color: Color.fromRGBO(0, 5, 132, 1.0)
          )
        )
      ),
      value: mapa['idvrsta']
    );

      dropdownTrainingTypes.add(novi);
    
    print("Added type to dropdown list :: " + mapa['nazivvrsta'].toString() + " " + mapa['idvrsta'].toString());
  }
  print(dropdownTrainingTypes.length);
}

Future<void> dropdownPrepareIntensities() async{

  if(intensities != null && dropdownTrainingIntensities.length == 0) for(dynamic mapa in intensities['intensities']){
    DropdownMenuItem novi = new DropdownMenuItem(
      child: Center(
        child: Text("   " +  mapa['nazivintenzitet'].toString(),
          style: new TextStyle(
            fontSize: 14.0,
            color: Color.fromRGBO(0, 5, 132, 1.0)
          )
        )
      ),
      value: mapa['idintenzitet']
    );

      dropdownTrainingIntensities.add(novi);
    
    print("Added intensity to dropdown list :: " + mapa['nazivintenzitet'].toString() + " " + mapa['idintenzitet'].toString());
  }
  print(dropdownTrainingIntensities.length);
}


  Future<void> getTrainingTypesData() async{

    print("U get training types sam !!!");
    prefs = await _getSharedPrefs();

    var response =  await http.get(
      Uri.parse(URI + '/mobile/termTypes'),
      headers: defaultHeader,  
    );

    if(response.statusCode == 200){
      
      types = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
      print("Status code 200 ! ");
    }

    
  }

Future<void> getTrainingIntensitiesData() async{

    print("U get training intensities sam !!!");
    prefs = await _getSharedPrefs();

    var response =  await http.get(
      Uri.parse(URI + '/mobile/termIntensities'),
      headers: defaultHeader,
    );

    if(response.statusCode == 200){
      
      intensities = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
      print("Status code 200 ! ");
    }

    
  }
String _selectedTimeStart;
String _selectedTimeEnd;
Future<void> _showStart() async {
    final TimeOfDay result = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (BuildContext context, Widget child) {
    return Theme(
      data: ThemeData.light().copyWith(
          primaryColor: const Color(0xFF0906A7),
          accentColor: const Color(0xFF0906A7),
          colorScheme: ColorScheme.light(primary: const Color(0xFF0906A7)),
          buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary
          ),
      ),
      child: child,
    );
  });
       if (result != null) {
      setState(() {
        _selectedTimeStart = result.format(context);
      });}
}

Future<void> _showEnd() async {
    final TimeOfDay result = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (BuildContext context, Widget child) {
    return Theme(
      data: ThemeData.light().copyWith(
          primaryColor: const Color(0xFF0906A7),
          accentColor: const Color(0xFF0906A7),
          colorScheme: ColorScheme.light(primary: const Color(0xFF0906A7)),
          buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary
          ),
      ),
      child: child,
    );
  },);
       if (result != null) {
      setState(() {
        _selectedTimeEnd = result.format(context);
      });}
}
  
  
  int _value = 1;
  int _value2 = 1;
  List<DropdownMenuItem<dynamic>> dropdownTrainingTypes = [];
  List<DropdownMenuItem<dynamic>> dropdownTrainingTypes2 = [];
  List<DropdownMenuItem<dynamic>> dropdownTrainingIntensities = [];
  List<DropdownMenuItem<dynamic>> dropdownTrainingIntensities2 = [];
  DateTime selectedDate = DateTime.now();

  @mustCallSuper
  void initState() {   

    (() async {


      await getTrainingTypesData();
      print("TYPES :::::: " + types.toString());
      await dropdownPrepareTypes();

      await getTrainingIntensitiesData();
      print("INTENSITIES :::::: " + intensities.toString());
      await dropdownPrepareIntensities();

    })();

    setState((){
      dropdownTrainingTypes2 = dropdownTrainingTypes;
      dropdownTrainingIntensities2 = dropdownTrainingIntensities;
    });
    super.initState();
  }

	@override
	Widget build(BuildContext context) {
    
		final Size screen = MediaQuery.of(context).size;

		return Scaffold(
			resizeToAvoidBottomInset: false,
			body: Container(
							
							constraints: BoxConstraints.expand(),
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("res/img/workout-bg.png"),
									fit: BoxFit.cover
						)),
			
              
							child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                
								children: [
                
                  // Spacer od vrha do linka za povratak na lokacije
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za povratak na lokacije
                  link(context, "ODABIR LOKACIJE", "left", LocationActivity()),

                  // Spacer od teksta "Odabir Lokacije" do teksta "ODABIR VRSTE TRENINGA"
                  SizedBox(height: screen.height * 0.017),

                  // Naslov "NOVI TERMIN"
                  if(prefs != null && prefs.getString('type') == "coach" && (responseData  == null || responseData['success'] == 1)) heading(context, "NOVI TERMIN"),

                  SizedBox(height: 40),



    Container(
  width: screen.width * 0.72,
  height: 40,
  margin: EdgeInsets.symmetric(vertical: 15),
  padding:
      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
  decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(20)),


  child:DropdownButtonHideUnderline(child: DropdownButton(
              focusColor:Colors.white,
              value: _value2,
              
              items: dropdownTrainingIntensities2,
              onChanged: (value2) {
                setState(() {
                  _value2 = value2;
                });
              })),
),

Container(
  width: screen.width * 0.72,
  height: 40,
  margin: EdgeInsets.symmetric(vertical: 15),
  padding:
      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
  decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(20)),

  // dropdown below..
  child:DropdownButtonHideUnderline(child: DropdownButton(
              focusColor:Colors.white,
              value: _value,
              
              items: dropdownTrainingTypes2,
              onChanged: (value) {
                setState(() {
                  _value = value;
                });
              })),
),
                
  SizedBox(height: MediaQuery.of(context).size.height/50),
        Divider(color: Colors.white),
        SizedBox(height: MediaQuery.of(context).size.height/50),

            Row(
  mainAxisSize: MainAxisSize.min,
  children: <Widget>[
    
    ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(0, 5, 132, 1.0)),
        onPressed: () => _selectDate(context), 
        child: Text(
          "${selectedDate.toLocal()}".split(' ')[0],
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
    ),
    SizedBox(width: 10),
    ElevatedButton(
            style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(0, 5, 132, 1.0)),
            onPressed: _showStart, 
            child: Text(_selectedTimeStart != null ? _selectedTimeStart : 'Vrijeme početak',
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
  SizedBox(width: 10),
    ElevatedButton(
            style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(0, 5, 132, 1.0)),
            onPressed: _showEnd, 
            child: Text(_selectedTimeEnd != null ? _selectedTimeEnd : 'Vrijeme kraj',
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),   

  ],
),

  SizedBox(height: MediaQuery.of(context).size.height/50),
        Divider(color: Colors.white),
        SizedBox(height: MediaQuery.of(context).size.height/50),

        MaterialButton(

                    minWidth: screen.width * 0.5,
                    height: screen.height * 0.05,
                    color: Colors.white,
                    disabledColor: Colors.white,

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0)
                    ),

                    child: new Text('UNESI TERMIN', 
                      style: new TextStyle(
                        fontFamily: "Product-Sans",
                        color: Color.fromRGBO(0, 5, 132, 1.0),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold
                      )),
                    
            onPressed: () async {
              
              var response = await createTerm(widget.locationId, _selectedTimeStart, _selectedTimeEnd, selectedDate, 1, 1,20);
              if(response.statusCode == 200){
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                if(responseData['success'] == 1) 
                Navigator.pushReplacement(
                  context,
                  PageRouteBuilder(

                    pageBuilder: (c, a1, a2) => ProfileActivity(),
                    transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 200)
              )); 
              }
                
            })






                  
          ]))
		);
  }
}
