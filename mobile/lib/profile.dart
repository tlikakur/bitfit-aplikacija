import 'package:flutter/material.dart';
import 'styles.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';
import 'locations.dart';
import 'main.dart';
import 'coachedTerms.dart';

class ProfileActivity extends StatefulWidget {
	ProfileActivity({Key key}) : super(key: key);

	@override
	_ProfileActivityState createState() => _ProfileActivityState();
}

class _ProfileActivityState extends State<ProfileActivity> {

  Map<String, dynamic> responseData;

  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);
	
  var prefs;

  Future<void> getProfileData() async{

    prefs = await _getSharedPrefs();

    var response =  await http.get(
      Uri.parse(URI + '/mobile/userInfo?id=' + prefs.getInt('id').toString() + '&type=' + prefs.getString('type')),
      headers: defaultHeader,  
    );

    if(response.statusCode == 200)
      responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
  }

  String firstName = "", lastName = "", dateOfBirth = "", oib = "", email = "", kif = "", istekclanarina="-";
  String termId = "", termDate = "", termStart = "", termEnd = "", termType = "", termReserved = "", termLocation = "";
  String termCoachFirstName = "", termCoachLastName = "", brodradenih = "", brtreninga = "";
  int termIntensity;
  double prosjDolaznost = 0.0;

  var done = false;


  @mustCallSuper
  void initState(){
    print("HELLO THERE!");
    (() async {
      await getProfileData();
      print("RESPONSE DATA: " + responseData.toString());
      setState(() {
        done = true;
        firstName = responseData['info']['imeosoba'];
        lastName = responseData['info']['prezimeosoba'];
        dateOfBirth = responseData['info']['datumrodenjaosoba'];
        if(responseData['info']['brodradenih'] != null) brodradenih = responseData['info']['brodradenih'];
        if(responseData['info']['brtreninga'] != null) brtreninga = responseData['info']['brtreninga'];
        oib = responseData['info']['oibosoba'];
        email = responseData['info']['emailosoba'];
        if(responseData['info']['ukdolaznost'] != null) prosjDolaznost = double.parse(responseData['info']['ukdolaznost'])/ int.parse(responseData['info']['brtreninga']);
        if(responseData['info']['istekclanarina'] != null) istekclanarina = responseData['info']['istekclanarina'];

        if(responseData['info']['zavrsenkif'] == 1) kif = "Da";
        if(responseData['info']['zavrsenkif'] == 0) kif = "Ne";

        if(responseData['term'] != null){

          termId = responseData['term']['idtermin'].toString();
          termDate = responseData['term']['datumtermin'];
          termStart = responseData['term']['vrijemepocetak'];
          termEnd = responseData['term']['vrijemezavrsetak'];
          termType = responseData['term']['nazivvrsta'];
          termIntensity = responseData['term']['idintenzitet'];
          termReserved = responseData['term']['brojrezerviranih'];
          termLocation = responseData['term']['nazivlokacija'];
          termCoachFirstName = responseData['term']['imeosoba'];
          termCoachLastName = responseData['term']['prezimeosoba'];
      }});
    })();
    super.initState();
  }

	@override
	Widget build(BuildContext context) {

		final Size screen = MediaQuery.of(context).size;

		return Scaffold(
			resizeToAvoidBottomInset: false,
			body: Container(
							
							constraints: BoxConstraints.expand(),
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("res/img/workout-bg.png"),
									fit: BoxFit.cover
						)),
			
              
							child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                
								children: [
                
                  // Spacer od vrha do linka za povratak na lokacije
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za odlazak na lokacije // kreaciju termina
                  if(prefs != null && prefs.getString('type') == "user") link(context, "LOKACIJE", "right", LocationActivity()),

                  if(prefs != null && prefs.getString('type') == "coach") link(context, "NOVI TERMIN", "right", LocationActivity()),

                  // Spacer od teksta "Lokacije" do teksta "VAŠ PROFIL"
                  SizedBox(height: screen.height * 0.017),

                  // Naslov "VAŠ PROFIL"
                  if(prefs != null) heading(context, "VAŠ PROFIL"),

                  // PODNASLOV "Informacije o računu"
                  if(done) heading2(context, "Informacije o računu"),

                  // Container za infromacije o  računu
                  if(done) Container(
                    padding: EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 15),
									  	margin: EdgeInsets.symmetric(horizontal: screen.width * 0.08, vertical: screen.height * 0.02),
									  	height: 180,
									  	width:double.infinity,
									  	decoration: BoxDecoration(
									  		color: Colors.white,
									  		borderRadius: BorderRadius.circular(30), //border corner radius
									  	),

                      child: Stack(
                        children: [ 
                        Positioned(right: -16, bottom: -16,
                        child: Opacity(opacity: 0.4,
			  	                child: Image(
                          image: AssetImage('res/img/stats-art.png'),
                          width: 130, height: 130, fit: BoxFit.fitHeight
              )
			  	)),

                        Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [

                          // Informacije o korisniku
                          Row(children:[Text("Ime i prezime:".padRight(20) + firstName + " " + lastName, style: termMainTextStyle)]),
                          Row(children:[Text("E-Mail:".padRight(25) + email, style: termMainTextStyle)]),
                          Row(children:[Text("OIB:".padRight(26) + oib, style: termMainTextStyle)]),
                          Row(children:[Text("Datum rođenja:".padRight(19) + dateOfBirth, style: termMainTextStyle)]),
                          if(prefs != null && prefs.getString('type') == "user") Row(children:[Text("Članarina  do:".padRight(21) + istekclanarina, style: termMainTextStyle)]),
                          if(prefs != null && prefs.getString('type') == "coach") Row(children:[Text("Završen KIF:".padRight(21) + kif, style: termMainTextStyle)]),
                          if(prefs != null && prefs.getString('type') == "user") Row(children:[Text("Odrađeni treninzi:".padRight(19) + brodradenih + "/" + brtreninga, style: termMainTextStyle)]),
                          if(prefs != null && prefs.getString('type') == "coach") Row(children:[Text("Vaši treninzi:".padRight(23) + brtreninga, style: termMainTextStyle)]),
                          if(prefs != null && prefs.getString('type') == "coach") Row(children:[Text("Prosj. dolaznost:".padRight(20) + prosjDolaznost.toStringAsFixed(2) + " korisnika / trening", style: termMainTextStyle)]),

                        ]
                      )])),

                  // Podnaslov rezervirani termin * Za korisnika *
                  if(done && prefs != null && prefs.getString('type') == "user" && termId != "") heading2(context, "Rezervirani termin"),

                  // Expansion Card sa informacijama o rezerviranom terminu * Za korisnika * 
                  if(done && prefs != null && prefs.getString('type') == "user" && termId != "") termCard(context, termId, termDate + " " + termStart + " - " + termEnd, termType, "Nema preporuka", termCoachFirstName + " " + termCoachLastName, termReserved, termIntensity, "OTKAŽI", termLocation, null),
                  
                  // Podnaslov nadolazeci termin * Za korisnika *
                  if(prefs != null && prefs.getString('type') == "coach" && termId != "") heading2(context, "Nadolazeći termin"),

                  // Expansion Card sa informacijama o terminu * Za korisnika * 
                  if(done && prefs != null && prefs.getString('type') == "coach" && termId != "") termCard(context, null, termDate + " " + termStart + " - " + termEnd, termType, "Nema preporuka", null, termReserved, termIntensity, null, termLocation, null),
                  
                  SizedBox(height: screen.height * 0.017),

                  // Gumb "POPIS TERMINA" za voditelja
                  if(done && prefs != null && prefs.getString('type') == "coach") MaterialButton(

                    minWidth: screen.width * 0.5,
                    height: screen.height * 0.05,
                    color: Colors.white,
                    disabledColor: Colors.white,

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0)
                    ),

                    child: new Text('POPIS TERMINA', 
                      style: new TextStyle(
                        fontFamily: "Product-Sans",
                        color: Color.fromRGBO(0, 5, 132, 1.0),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold
                      )),
                    
            onPressed: () async {

              // Jump to coachedTerms activity
              Navigator.pushReplacement(
                  context, 
                  PageRouteBuilder(

                    pageBuilder: (c, a1, a2) => CoachedTermsActivity(),
                    transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 200)
              ));      
            }),

                  SizedBox(height: screen.height * 0.017),

                  // Gumb "Prijava"
                 if(done && prefs != null)  MaterialButton(

                    minWidth: screen.width * 0.5,
                    height: screen.height * 0.05,
                    color: Colors.white,
                    disabledColor: Colors.white,

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0)
                    ),

                    child: new Text('ODJAVA', 
                      style: new TextStyle(
                        fontFamily: "Product-Sans",
                        color: Color.fromRGBO(0, 5, 132, 1.0),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold
                      )),
                    
            onPressed: () async {
              
              SharedPreferences prefs = await SharedPreferences.getInstance();
              await prefs.clear();

              // Display success msg after clearing sharedPrefs
              dynamic snackBar = snackbar(context, Color.fromRGBO(55, 95, 163, 0.7), "Uspješno ste se odjavili iz sustava.");
              ScaffoldMessenger.of(context).showSnackBar(snackBar);

              // Jump to login
              Navigator.pushReplacement(
                  context, 
                  PageRouteBuilder(

                    pageBuilder: (c, a1, a2) => LoginActivity(),
                    transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 200)
              ));      
            })

        ])),
		);
	}
}