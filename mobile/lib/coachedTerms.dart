import 'package:flutter/material.dart';
import 'styles.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';
import 'profile.dart';


class CoachedTermsActivity extends StatefulWidget {

	CoachedTermsActivity({Key key}) : super(key: key);

	@override
	_CoachedTermsActivityState createState() => _CoachedTermsActivityState();
}

class _CoachedTermsActivityState extends State<CoachedTermsActivity> {

     
  Map<String, dynamic> responseData;

  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
  json.decode(utf8.decode(codeUnits), reviver: reviver);

	var done = false;
  var prefs;
  

  Future<void> getCoachedTerms() async{

    prefs = await _getSharedPrefs();
    var response =  await http.get(
      Uri.parse(URI + '/mobile/termsForCoach?coachid=' + prefs.getInt('id').toString()),
      headers: defaultHeader,  
    );

    if(response.statusCode == 200) responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
  }

  List<dynamic> upcomingTerms;
  List<dynamic> activeOrFinishedTerms;

  @mustCallSuper
  void initState(){
    (() async {
      await getCoachedTerms();
      print("RESPONSE DATA: " + responseData.toString());
      setState(() {

        upcomingTerms = responseData['upcomingTerms'];
        activeOrFinishedTerms = responseData['activeOrFinishedTerms'];
        done = true;
        print("Upcoming terms: " + upcomingTerms.toString());
        print("Active or finished: " + activeOrFinishedTerms.toString());
      });
    })();
    super.initState();
  }
	
	@override
	Widget build(BuildContext context) {

		final Size screen = MediaQuery.of(context).size;

		return Scaffold(
			resizeToAvoidBottomInset: false,
			body: Container(
							
							constraints: BoxConstraints.expand(),
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("res/img/workout-bg.png"),
									fit: BoxFit.cover
						)),
			
              child: SingleChildScrollView(
							child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                
								children: [
                
                  // Spacer od vrha do linka za povratak na lokacije
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za povratak na lokacije
                  link(context, "VAŠ PROFIL", "left", ProfileActivity()),
         

                  // Spacer od teksta "Lokacije" do teksta "ODABIR TERMINA"
                  SizedBox(height: screen.height * 0.017),

                  // Naslov "TERMINI - Ime Lokacije"
                  if(prefs != null && prefs.getString('type') == "coach" && responseData['success'] == 1) heading(context, "POPIS MOJIH TERMINA"),


                  heading2(context, "Nadolazeći termini"),
                  // Expansion Card sa informacijama o terminu
                  if(upcomingTerms != null && upcomingTerms.length > 0) for(Map<String, dynamic> term in upcomingTerms)
                  termCard(
                    context, 
                    term['idtermin'].toString(),
                    term['datumtermin'] + " " + term['vrijemepocetak'] + 'h - ' + term['vrijemezavrsetak'] + 'h',
                    term['nazivvrsta'],
                    "Nema preporuka", 
                    null, // Voditelj
                    term['brojclanova'].toString() + "/" + term['maxbrojclanova'].toString(),
                    term['idintenzitet'],
                    "POPIS KORISNIKA",
                    term['nazivlokacija'], null),

                  heading2(context, "Završeni termini"),
                  // Expansion Card sa informacijama o terminu
                  if(activeOrFinishedTerms != null && activeOrFinishedTerms.length > 0) for(Map<String, dynamic> term in activeOrFinishedTerms)
                  termCard(context, term['idtermin'].toString(), term['datumtermin'] + " " + term['vrijemepocetak'] + 'h - ' + term['vrijemezavrsetak'] + 'h', term['nazivvrsta'], "Nema preporuka", null, term['brojclanova'].toString() + "/" + term['maxbrojclanova'].toString(), term['idintenzitet'], "POPIS KORISNIKA", null, null),


          
                  // If loading is done and there are no available terms
                  if(done && (activeOrFinishedTerms == null  || activeOrFinishedTerms.length == 0) && (upcomingTerms == null  || upcomingTerms.length == 0))
                    // TO DO: Move no-items-message to Styles
                    Column(
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/5),
                        Image.asset('res/icons/empty-icon.png',scale:6),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                        Text("NEMA PRETHODNIH TERMINA :C",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 16)),
                        SizedBox(height: MediaQuery.of(context).size.height/75),
                        Text("Molimo odaberite neku drugu lokaciju.",
                        style: TextStyle(color:Colors.white, fontSize: 13)),



            ],

          ),

          ]))),
		);
	}
}