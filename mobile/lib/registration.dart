import 'package:flutter/material.dart';
import 'main.dart';
import 'styles.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'extras.dart';


class RegistrationActivity extends StatefulWidget {
  RegistrationActivity({Key key}) : super(key: key);

  @override
  _RegistrationActivityState createState() => _RegistrationActivityState();
}

class _RegistrationActivityState extends State<RegistrationActivity> {
  

 Map<String, dynamic> responseData;

  TextEditingController emailControllerTF = new TextEditingController();
  TextEditingController passwordControllerTF = new TextEditingController();
  TextEditingController oibControllerTF = new TextEditingController();
  TextEditingController firstNameControllerTF = new TextEditingController();
  TextEditingController lastNameControllerTF = new TextEditingController();
  TextEditingController dobControllerTF = new TextEditingController();

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

  Future<http.Response> loginRequest() async{
    return http.post(
      Uri.parse(URI+'/mobile/register'),
      headers: defaultHeader,
      body: jsonEncode(<String, dynamic>{

        'email': emailControllerTF.text,
        'password': passwordControllerTF.text,
        'oib': oibControllerTF.text,
        'first_name': firstNameControllerTF.text,
        'last_name': lastNameControllerTF.text,
        'date_of_birth': dobControllerTF.text
      }),    
    );
  }

  bool _passObscure = true;

  void _toggle() {
    setState(() {
      _passObscure = !_passObscure;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screen = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
              
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("res/img/workout-bg.png"),
                  fit: BoxFit.cover
            )),
      
              child: Column(
                children: [

                  // Spacer od vrha do linka za povratak na prijavu
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za povratak na prijavu
                  GestureDetector(
                    onTap: () {
                      Navigator.pushReplacement(
                        context, 
                        PageRouteBuilder(
                          pageBuilder: (c, a1, a2) => LoginActivity(),
                          transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 200),
                        ) 
                      );
                    },

                    child:
                      Row(
                        children:[
                          SizedBox(width: screen.width * 0.03), // Horizontalni spacer
                          Icon(Icons.arrow_back_ios, color: Colors.white, size:12),
                          Text('PRIJAVA', style: linkBackStyle)
                        ]
                    )),

                  // Spacer od teksta "Prijava" do logotipa BITFIT
                  SizedBox(height: screen.height * 0.05),

                  // Logotip BITFIT
                  Image.asset('res/img/bitfit-logo.png', height: screen.height * 0.17),

                  // TextField "Ime"
                  Container(

                    width: screen.width * 0.8,
                    child: TextField(
                      controller: firstNameControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  Ime',   
                        hintStyle: basicTextField,
                        contentPadding: EdgeInsets.only(top: 10),
                        
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                  // TextField "Prezime"
                  Container(

                    width: screen.width * 0.8,
                    child: TextField(
                      controller: lastNameControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  Prezime',   
                        hintStyle: basicTextField,
                        contentPadding: EdgeInsets.only(top: 20),
                              
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                  // TextField "OIB"
                  Container(

                    width: screen.width * 0.8,
                    child: TextField(
                      controller: oibControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  OIB',   
                        hintStyle: basicTextField,
                        contentPadding: EdgeInsets.only(top: 20),
                              
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                  // TextField "E-Mail"
                  Container(

                    width: screen.width * 0.8,
                    child: TextField(
                      controller: emailControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  E-Mail',   
                        hintStyle: basicTextField,
                        contentPadding: EdgeInsets.only(top: 20),
                              
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                  // TextField "Datum rodenja"
                  Container(

                    width: screen.width * 0.8,
                    child: TextField(
                      controller: dobControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  Datum rođenja',   
                        hintStyle: basicTextField,
                        contentPadding: EdgeInsets.only(top: 20),
                              
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                  // TextField "Lozinka"
                  Container(
                    
                    width: screen.width * 0.8,
                    child: TextField(
                      obscureText: _passObscure,
                      enableSuggestions: false,
                      autocorrect: false,
                      controller: passwordControllerTF,
                      style: basicTextField,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(     
                        hintText: '  Lozinka',   
                        hintStyle: basicTextField,
                        suffixIcon: IconButton(
                          iconSize: 24,
                          padding: EdgeInsets.only(top:15),
                          icon: Icon(Icons.remove_red_eye, color: Colors.white),
                            onPressed: _toggle
                        ),

                        contentPadding: EdgeInsets.only(top: 20),
                              
                        enabledBorder: UnderlineInputBorder(      
                          borderSide: BorderSide(color: Colors.white),   
                        ),  

                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),

                        border: UnderlineInputBorder(     
                          borderSide: BorderSide(color: Colors.white),
                        ),
                      )

                  )),

                   // Spacer od textfielda do gumba
                  SizedBox(height: screen.height * 0.1),

                  // Gumb "Registracija"
                  MaterialButton(

                    minWidth: screen.width * 0.5,
                    height: screen.height * 0.05,
                    color: Color.fromRGBO(229, 229, 229, 1.0),
                    disabledColor: Colors.white,

                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0)
                    ),

                    child: new Text('REGISTRACIJA', 
                      style: new TextStyle(
                        fontFamily: "Product-Sans",
                        color: Color.fromRGBO(0, 5, 132, 1.0),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold
                    )),
                    
                    onPressed: () async {

              
              var response = await loginRequest();
              var msgStart = "";
              var bgColor = Color.fromRGBO(55, 95, 163, 0.7);

              


              if(response.statusCode == 200){

                
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                print("RESPONSE DATA: " + responseData.toString());

                if(responseData['success'] == 0){
                  msgStart = "Greška: ";
                  bgColor = Color.fromRGBO(222, 47, 47, 0.7);
                  


                }

                dynamic snackBar = SnackBar(margin: EdgeInsets.only(bottom: 50, left: 30, right: 30),
                                              behavior: SnackBarBehavior.floating,
                                              backgroundColor: bgColor,
                                              content: Text(msgStart + responseData['msg'],
                                              style: TextStyle(color: Colors.white),
                                              textAlign: TextAlign.center),
                                              duration: Duration(milliseconds: 1500),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)));

                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                // If login is successful
                if(responseData['success'] == 1){
                  

                  Navigator.pushReplacement(
                  context, 
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => LoginActivity(),
                    transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 200)
                  ));
                }      

              }
            },     
          ),
        ],
      ),
      ),
    );
  }
}