import 'package:flutter/material.dart';
import 'styles.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';
import 'coachedTerms.dart';


class UsersListActivity extends StatefulWidget {
  final int termid;
	UsersListActivity({Key key, @required this.termid}) : super(key: key);

	@override
	_UsersListActivityState createState() => _UsersListActivityState();
}

class _UsersListActivityState extends State<UsersListActivity> {

     
    Map<String, dynamic> responseData;

  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

	var done = false;
  var prefs;
  

  Future<void> getUsersOnTerm() async{

    prefs = await _getSharedPrefs();
    print('/mobile/usersOnTerm?termid=' + widget.termid.toString());
    var response =  await http.get(
      Uri.parse(URI + '/mobile/usersOnTerm?termid=' + widget.termid.toString()),
      headers: defaultHeader,  
    );

    if(response.statusCode == 200)
      responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
  }

  List<dynamic> users;

  @mustCallSuper
  void initState(){
    (() async {
      await getUsersOnTerm();
      print("RESPONSE DATA: " + responseData.toString());
      setState(() {

        users = responseData['users'];
        done = true;
        print("Users: " + users.toString());
      });
    })();
    super.initState();
  }
	
	@override
	Widget build(BuildContext context) {

		final Size screen = MediaQuery.of(context).size;

		return Scaffold(
			resizeToAvoidBottomInset: false,
			body: Container(
							
							constraints: BoxConstraints.expand(),
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("res/img/workout-bg.png"),
									fit: BoxFit.cover
						)),
			
              
							child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                
								children: [
                
                  // Spacer od vrha do linka za povratak na popis termina
                  SizedBox(height: screen.height * 0.06),

                  // Tekst (tj. link) za povratak na popis termina
                  link(context, "TERMINI", "left", CoachedTermsActivity()),
         

                  // Spacer od linka "TERMINI" do teksta naslova
                  SizedBox(height: screen.height * 0.017),

                  // Naslov
                  if(prefs != null && prefs.getString('type') == "coach" && responseData['success'] == 1) heading(context, "KORISNICI - TERMIN BR. " + widget.termid.toString()),


                  // Card sa informacijama o korisniku
                  if(users != null && users.length != 0) for(Map<String, dynamic> user in users)
                  
                    GestureDetector(
            onTap: () async {
              print("Desi");
              var status = 0;
              user['statusdolaska'] == 0 ? status = 1 : status = 0;
              var response = await setArrival(user['idkorisnik'], widget.termid, status);
              if(response.statusCode == 200){
                dynamic responseDataUser = await jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                setState(() {
                  if(responseDataUser['success'] == 1){
                  if(user['statusdolaska'] == 0) user['statusdolaska'] = 1;
                  else if(user['statusdolaska'] == 1) user['statusdolaska'] = 0;
                }
             
                });

              }

            },

            child: Container(
			padding: EdgeInsets.only(left: 20, top: 10),
			margin: EdgeInsets.only(left: screen.width * 0.08, right: screen.width * 0.08, top: screen.height * 0.02, bottom: screen.height * 0.02),
			height: 80,
			width:double.infinity,
			decoration: BoxDecoration(
			  color: Colors.white,
			  borderRadius: BorderRadius.circular(25), //border corner radius
			),

			child:Stack(
        children: [
          // Slika
			  	Positioned(right: screen.width * 0.25, bottom: 0,
            child: Opacity(opacity: 0.4,
			  	    child: Image(
                image: AssetImage('res/img/stats-art.png'),
                width: 80, height: 80, fit: BoxFit.fitHeight
              )
			  	)),

			  	// Ime lokacije
			  	Positioned(top:10,child:Text(user['imeosoba'] + " " + user['prezimeosoba'], style: locationMainTextStyle)),

			  	// Opis lokacije
			  	Positioned(top: 30, child:Text(user['emailosoba'], style: locationAdditionalTextStyle)),

          Positioned(top: 12, right: 25, child: SizedBox(width: 35, height:35, child: Container(
            
          child: SizedBox(
    width: 20,
    child: IconButton(
    icon: user['statusdolaska'] == 1 ? Icon(Icons.check, color: Colors.blue[900]) : Icon(Icons.remove_circle_outline, color: Colors.blue[900]),
    iconSize: 25,    
    onPressed: null,
   ),
  ),
     
        
)))
			  ]),
		)),
                  
                  
                  // Ako je ucitavanje gotovo i nema korisnika
                  if(done && prefs != null && (users == null  || users.length == 0))
                    // TO DO: Move no-items-message to Styles
                    Column(
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/5),
                        Image.asset('res/icons/empty-icon.png',scale:6),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                        Text("NEMA REZERVACIJA ZA OVAJ TERMIN :C",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 16)),
                        SizedBox(height: MediaQuery.of(context).size.height/75),




            ],

          ),

          ])),
		);
	}
}