import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'styles.dart';
import 'dart:convert';
import 'registration.dart';
import 'profile.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'extras.dart';





void main() async{
  
    runApp(MyApp());

}


class MyApp extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
  
    // Postavljanje boja navigacijske i status trake
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: Color.fromRGBO(40, 40, 40, 1),
        statusBarColor: Color.fromRGBO(70, 70, 70, 0.6)
    ));

    return MaterialApp(

      // Micanje debug trake
      debugShowCheckedModeBanner: false,

      home: LoginActivity(),

    );

  }
}

class LoginActivity extends StatefulWidget {
  LoginActivity({Key key}) : super(key: key);

  @override
  _LoginActivityState createState() => _LoginActivityState();
}

class _LoginActivityState extends State<LoginActivity> {


  Map<String, dynamic> responseData;


  bool _passObscure = true;

  void _toggle() {
    setState(() {
      _passObscure = !_passObscure;
    });
  }

  TextEditingController emailControllerTF = new TextEditingController();
  TextEditingController passwordControllerTF = new TextEditingController();


  dynamic _getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;

  }

  dynamic jsonDecodeUtf8(List<int> codeUnits, {Object reviver(Object key, Object value)}) =>
    json.decode(utf8.decode(codeUnits), reviver: reviver);

  var done;

  Future<http.Response> loginRequest() async{
    return http.post(
      Uri.parse(URI+'/mobile/login'),
      headers: defaultHeader,
      body: jsonEncode(<String, dynamic>{
        'email': emailControllerTF.text,
        'password': passwordControllerTF.text
      }),    
    );
  }


  @override
  Widget build(BuildContext context) {

    final Size screen = MediaQuery.of(context).size;
    
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("res/img/workout-bg.png"),
            fit: BoxFit.cover
          )
        ),
      
      child: Column(

        children: [

          // Spacer od vrha do logotipa BITFIT
          SizedBox(height: screen.height * 0.22),

          // Logotip BITFIT
          Image.asset('res/img/bitfit-logo.png', height: screen.height * 0.25),

          // TextField "E-Mail"
          Container(
                 
            width: screen.width * 0.8,
            child: TextField(
              controller: emailControllerTF,  
              style: basicTextField,
              cursorColor: Colors.white,
              decoration: InputDecoration(     
                hintText: '  E-Mail',   
                hintStyle: basicTextField,
                contentPadding: EdgeInsets.only(top: 10),

                enabledBorder: UnderlineInputBorder(      
                  borderSide: BorderSide(color: Colors.white),   
                ),  

                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),

                border: UnderlineInputBorder(     
                  borderSide: BorderSide(color: Colors.white),
                ),
              )

            )),

          // TextField "Lozinka"
          Container(

            width: screen.width * 0.8,
            child: TextField(
              obscureText: _passObscure,
              enableSuggestions: false,
              autocorrect: false,
              controller: passwordControllerTF,  
              style: basicTextField,
              cursorColor: Colors.white,
              decoration: InputDecoration(     
                hintText: '  Lozinka',   
                hintStyle: basicTextField,
                contentPadding: EdgeInsets.only(top: 20),
                suffixIcon: IconButton(
                  iconSize: 24,
                  padding: EdgeInsets.only(top:15),
                  icon: Icon(Icons.remove_red_eye, color: Colors.white),
                  onPressed: _toggle
),
                              
                  enabledBorder: UnderlineInputBorder(      
                    borderSide: BorderSide(color: Colors.white),   
                  ),  

                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),

                  border: UnderlineInputBorder(     
                    borderSide: BorderSide(color: Colors.white),
                  ),
              )

            )),

          // Spacer od textfielda do gumba
          SizedBox(height: screen.height * 0.1),

          // Gumb "Prijava"
          MaterialButton(

            minWidth: screen.width * 0.5,
            height: screen.height * 0.05,
            color: Color.fromRGBO(229, 229, 229, 1.0),
            disabledColor: Colors.white,

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0)
            ),

            child: new Text('PRIJAVA', 
              style: new TextStyle(
                fontFamily: "Product-Sans",
                color: Color.fromRGBO(0, 5, 132, 1.0),
                fontSize: 16.0,
                fontWeight: FontWeight.bold
              )),
                    
            onPressed: () async {

              var prefs = await  _getSharedPrefs();
              var response = await loginRequest();

              if(response.statusCode == 200){

                
                responseData = jsonDecodeUtf8(response.bodyBytes).cast<String, dynamic>();
                print("RESPONSE DATA: " + responseData.toString());

                // If login is successful
                if(responseData['success'] == 1){

                  prefs.setString('type', responseData['acc']['type']);
                  prefs.setInt('id', responseData['acc']['id']);

                  Navigator.pushReplacement(
                  context, 
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => ProfileActivity(),
                    transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                    transitionDuration: Duration(milliseconds: 200)
                  ));

                }

                // If login is not successful
                else{

                  dynamic snackBar = snackbar(context, Color.fromRGBO(222, 47, 47, 0.7), "Greška: " + responseData['msg']);
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);

                }
              }
              
            },
                    
          ),

          // Spacer od gumba do textfielda
          SizedBox(height: screen.height * 0.015),

          // Tekst (tj. link) za odlazak na registraciju
          GestureDetector(
            onTap: () {
              Navigator.pushReplacement(
                context, 
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => RegistrationActivity(),
                  transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 200),
                ));
            },

            child:
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:[
                Text('Niste registrirani? ', style: basicTextField),
                Text('Registracija', style: basicTextField),
              ]
            )),
        ])
      )
    );
    }
  }

