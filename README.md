## BSc Thesis Project
### BITFIT Application
Main focus of this work is creation of a database, web application and a mobile application that
are intended for managing and usage of fitness centers. The database stores data about users,
coaches, locations, trainings and other information necessary for the successful operation of
this system. The web application is intended for employees to view, insert and delete data,
while the mobile application is inteded for users to view information, and for training spot
reservations. It's also intended for employees to view data, track user arrivals and insert new
appointments.
