process.env.PWD = process.cwd()

const express = require('express');
const path = require('path');
var session = require('express-session');

const functions = require('./extras/functions');
const logger = require('./extras/logger');
const settings = require('./extras/settings');


const app = express();

app.use(session({
	secret: 'bitfit',
	resave: true,
	saveUninitialized: true,
	cookie  : {
        //httpOnly: true,
        //maxAge  : 60 * 60 * 1000 ,
		//secure: true
    }
}));


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/style'));
app.use(express.static(process.env.PWD + '/public'));


const homeRouter = require('./routes/home.routes.js');
app.use('/', homeRouter);

const usersRouter = require('./routes/users.routes');
app.use('/users', usersRouter);

const coachesRouter = require('./routes/coaches.routes');
app.use('/coaches', coachesRouter);

const termsRouter = require('./routes/terms.routes');
app.use('/terms', termsRouter);

const extrasRouter = require('./routes/extras.routes');
app.use('/extras', extrasRouter); 

const employeesRouter = require('./routes/employees.routes');
app.use('/employees', employeesRouter); 

const mobileRouter = require('./routes/mobile.routes');
app.use('/mobile', mobileRouter);

const statsRouter = require('./routes/stats.routes');
app.use('/stats', statsRouter);


app.listen(settings.PORT, ()=> logger.actions.info(`${functions.dateTime()} - Application Started (PORT: ${settings.PORT})`));
