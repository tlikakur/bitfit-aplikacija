var express = require('express');

const { QueryTypes, Sequelize } = require('sequelize');
const { Korisnik, Osoba, Clanarina, sequelize } = require('../extras/sequelize');

var Joi = require('@hapi/joi');

const bcrypt = require('bcrypt');
const saltRounds = 10;


var router = express.Router();

var functions = require('../extras/functions');
var logger = require('../extras/logger');
var settings = require('../extras/settings');

router.use(express.json());
router.use(express.urlencoded({extended: true}));

// GET Request for login page
router.get('/', function (req, res, next) {

     // Render required page
     if(!req.session.loggedin) res.render('login', {title: 'Home', active: null, greska: req.app.locals.greska, success: req.app.locals.success});
     else res.redirect('/users');

     req.app.locals.greska = null;
     req.app.locals.success = null;
});


router.post('/login', async(req,res,next) => {

	// Get all users & their membership expiration
     var pass_db = await sequelize.query(
     `SELECT idOsoba, imeOsoba, prezimeOsoba, lozinkaOsoba, emailOsoba, jeAdmin FROM Osoba NATURAL JOIN Zaposlenik
     WHERE emailOsoba = '${req.body.email}'`,
     { type: QueryTypes.SELECT });


	if(pass_db != undefined && pass_db.length != 0){


		if(await bcrypt.compare(req.body.password, pass_db[0].lozinkaosoba)){
	
			if(pass_db[0].jeadmin == "1")
                    req.session.adminflag = true;
               
               else req.session.adminflag = false;
			req.session.loggedin = true;
			req.session.email = pass_db[0].emailosoba;
               req.session.imeprezime = pass_db[0].imeosoba + " " + pass_db[0].prezimeosoba;

			res.redirect('/home');

		}

		else{
	
        	     res.render('login', { title: 'BITFIT - Prijava' , greska: "Neispravno korisnicko ime ili lozinka!", active: null, success: null});
		}
	}

	else
    	     res.render('login', { title: 'BITFIT - Prijava' , greska: "Neispravno korisnicko ime ili lozinka!", active: null, success: null});
	
});

// Klik na logout gumb
router.get('/logout', function(req,res,next){

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          logger.actions.info(functions.dateTime() + " - Odjava zaposlenika | Zaposlenik: " + req.session.email);
          req.session.destroy()
        
          req.app.locals.success = `Uspješno ste se odjavili iz sustava.`;
          res.redirect('/');
     }
    
});

// HOMEPAGE
router.get('/home', function(req, res, next){

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
  
        
     res.render('home', {title: 'BITFIT - Početna', active: "none", 
                         adminflag: req.session.adminflag,  greska: req.app.locals.greska, 
                         success: req.app.locals.success, 
                         zaposlenik: req.session.imeprezime});

     
     
    
});

module.exports = router;
