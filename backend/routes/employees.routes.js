const express = require('express');

const { QueryTypes } = require('sequelize');
const { Zaposlenik, Osoba, Clanarina, sequelize } = require('../extras/sequelize');

const Joi = require('@hapi/joi');

const router = express.Router();

const bcrypt = require('bcrypt');
const saltRounds = 10;

const functions = require('../extras/functions');
const logger = require('../extras/logger');
const settings = require('../extras/settings');

router.use(express.json());
router.use(express.urlencoded({extended: true}));

// VALIDATION Schema
const employeeSchema = Joi.object().keys({

    email: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).email().required()
	.messages(settings.VALIDATION.emailMessages),

    first_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.firstNameMessages),

	last_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.lastNameMessages),

     oib: Joi.string().length(11).pattern(/^[0-9]+$/).required()
     .messages(settings.VALIDATION.oibMessages),

     password: Joi.string().min(settings.RULES.minPasswordLength).max(settings.RULES.maxPasswordLength).required()
     .messages(settings.VALIDATION.passwordMessages),

     date_of_birth: Joi.string().pattern(/^((0[1-9])|(1[\d])|(2[\d])|(3[01]))[.]((0[1-9])|(1[12]))[.]([\d]{4})[.]$/).required()
     .messages(settings.VALIDATION.dateOfBirthMessages)
});


// GET Request to display all employees
router.get('/', async function (req, res, next) {

     
    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
    else if(!req.session.adminflag){

        req.app.locals.greska = `Za ovu akciju potrebne su administratorske ovlasti!`;
        res.redirect('/users');

     }
     else{

          // Get all employees
          var zaposlenici = await sequelize.query(
          `SELECT Zaposlenik.idZaposlenik, idOsoba, imeOsoba, prezimeOsoba, jeAdmin,
           to_char(datumRodenjaOsoba::DATE, 'dd.mm.yyyy.') as datumRodenjaOsoba, oibOsoba, emailOsoba 
	      FROM Zaposlenik NATURAL JOIN Osoba`,
          { type: QueryTypes.SELECT });

          // Display required page
          res.render('employees', {title: 'BITFIT - Korisnici', active: "Zaposlenici", adminflag: req.session.adminflag,  greska: req.app.locals.greska, zaposlenici: zaposlenici, success: req.app.locals.success, zaposlenik: req.session.imeprezime});

          // Reset error & success messages after displaying
          req.app.locals.success = null;
          req.app.locals.greska = null;
     }
});



// POST request to remove employee
router.post('/removeEmployee', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
    else if(!req.session.adminflag){

        req.app.locals.greska = `Za ovu akciju potrebne su administratorske ovlasti!`;
        res.redirect('/users');

     }
    else{

        await Osoba.destroy({where: {idosoba: req.body.personid}});

	     // Setting success message
	     req.app.locals.success = `Zaposlenik (ID: ${req.body.employeeid}) uspješno obrisan iz sustava!`

	     // Logging into actions.txt
	     logger.actions.info(`${functions.dateTime} - Removed employee ${req.body.employeeid}`);

	     // Render required page
          res.redirect('/employees');
     }
});

// Post Request to insert employee
router.post('/insertEmployee', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
    else if(!req.session.adminflag){

        req.app.locals.greska = `Za ovu akciju potrebne su administratorske ovlasti!`;
        res.redirect('/users');

     }
    else{

          // Validation DATA
	     let data = {

               password: req.body.password,
               email: req.body.email, 
               date_of_birth: req.body.date_of_birth, 
               oib: req.body.oib, 
               first_name: req.body.first_name, 
               last_name: req.body.last_name
          };

          // Validation
	     var status = employeeSchema.validate(data, {abortEarly: false});

          // If validation is not OK
	     if(status.error)
		     req.app.locals.greska = status.error.details[0].message;

          // If validation is OK
          else{

               // Check if EMAIL or OIB exists in database
               var persons = await sequelize.query(`SELECT * FROM Osoba WHERE oibOsoba = ${req.body.oib} OR emailOsoba = '${req.body.email}';`,
               { type: QueryTypes.SELECT });

               
               if(persons.length == 0){

	          try{

                    var hash = await bcrypt.hash(req.body.password, saltRounds);
    
                    
                    // Inserting person into database
                    await sequelize.query(
                    `INSERT INTO Osoba (emailOsoba, imeOsoba, prezimeOsoba, oibOsoba, lozinkaOsoba, datumRodenjaOsoba) VALUES
                    ('${req.body.email}','${req.body.first_name}','${req.body.last_name}','${req.body.oib}','${hash}','${req.body.date_of_birth}');`,
                    { type: QueryTypes.INSERT });


                    // Get ID from person (FK for Employee Column)
                    var query = await sequelize.query(`SELECT idOsoba FROM Osoba WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT});
     
                    // Inserting employee into database
                    await sequelize.query(
                    `INSERT INTO Zaposlenik (idosoba, jeadmin) VALUES (${query[0].idosoba},0);`,
                    { type: QueryTypes.INSERT });

                    // Set success message
                    req.app.locals.success = `Zaposlenik (${req.body.email}) uspješno upisan u bazu podataka.`

                    // Logging into actions.txt
	               logger.actions.info(`${functions.dateTime} - Added employee ${req.body.email}`);
                    

               }catch(e){ 
                    console.log(e); 
                    req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`;
               }

               }

               else req.app.locals.greska = `Osoba sa ovim e-mailom ili OIB-om je već u bazi!`;

          
               // Render required page
               res.redirect('/employees');
          }
     }
});

// TODO: Add functions for giving & removing admin permissions
// Post Request to insert employee
router.post('/promoteEmployee', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
    else if(!req.session.adminflag){

        req.app.locals.greska = `Za ovu akciju potrebne su administratorske ovlasti!`;
        res.redirect('/users');

    }

    else{

        try{
            var status = await sequelize.query(
                `UPDATE Zaposlenik SET jeAdmin = 1 WHERE idZaposlenik = ${req.body.employeeid};`,
                { type: QueryTypes.SELECT });
        
             req.app.locals.success = `Zaposlenik (ID: ${req.body.employeeid}) uspješno promoviran u administratora!`

        }catch(err){
            console.log(err);
            req.app.locals.greska = `Dogodila se greška.`
        }

	    // Render required page
        res.redirect('/employees');
     }
});


module.exports = router;