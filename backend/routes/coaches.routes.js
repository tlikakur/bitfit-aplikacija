const express = require('express');

const { QueryTypes } = require('sequelize');

const router = express.Router();

const Joi = require('@hapi/joi');

const bcrypt = require('bcrypt');
const saltRounds = 10;
const functions = require('../extras/functions');
const logger = require('../extras/logger');
const settings = require('../extras/settings');

const { Voditelj, Osoba, sequelize, Korisnik } = require('../extras/sequelize')

router.use(express.json());
router.use(express.urlencoded({extended: true}));

// VALIDATION Schema
const coachSchema = Joi.object().keys({

    email: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).email().required()
	.messages(settings.VALIDATION.emailMessages),

    first_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.firstNameMessages),

	last_name: Joi.string().regex(/^[a-z ,.'-]+$/i).min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.lastNameMessages),

     oib: Joi.string().length(11).pattern(/^[0-9]+$/).required()
     .messages(settings.VALIDATION.oibMessages),

     password: Joi.string().min(settings.RULES.minPasswordLength).max(settings.RULES.maxPasswordLength).required()
     .messages(settings.VALIDATION.passwordMessages),

     date_of_birth: Joi.string().pattern(/^((0[1-9])|(1[\d])|(2[\d])|(3[01]))[.]((0[1-9])|(1[12]))[.]([\d]{4})[.]$/).required()
     .messages(settings.VALIDATION.dateOfBirthMessages)
});



router.get('/', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get all coaches from database
          var voditelji = await sequelize.query(
          "SELECT idVoditelj, idOsoba, imeOsoba, prezimeOsoba, to_char(datumRodenjaOsoba::DATE, 'dd.mm.yyyy.') AS datumRodenjaOsoba, oibOsoba, emailOsoba, zavrsenKif FROM Voditelj NATURAL JOIN Osoba;",
          { type: QueryTypes.SELECT });

          // Render required page
          res.render('coaches', {title: 'BITFIT - Voditelji', active: "Voditelji", adminflag: req.session.adminflag, greska: req.app.locals.greska, voditelji: voditelji, success: req.app.locals.success, zaposlenik: req.session.imeprezime});
     
          // Reset messages
          req.app.locals.success = null;
          req.app.locals.greska = null;
     }
});


router.get('/insert', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get users (available for promotion) from database
          var korisnici = await sequelize.query(
          "SELECT idKorisnik, idOsoba, imeOsoba, prezimeOsoba, oibOsoba FROM Korisnik NATURAL JOIN Osoba;",
          { type: QueryTypes.SELECT });

     

          // Render required page
          res.render('coachInsert', {title: 'BITFIT - Voditelji', adminflag: req.session.adminflag, active: "Voditelji", korisnici: korisnici, greska: req.app.locals.greska, success: req.app.locals.success, zaposlenik: req.session.imeprezime});

          // Reset success & error messages
          req.app.locals.greska = null;
          req.app.locals.success = null;
     }
}),


router.post('/info', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Depending if we come to INFO from Modal or from Table function button
          if(!req.query.coachid) req.query.coachid = req.body.coachid;

          // Get coach from database
          var voditelj = await sequelize.query(`SELECT *, to_char(datumRodenjaOsoba::DATE, 'dd.mm.yyyy.') AS datumRodenjaOsoba FROM Voditelj NATURAL JOIN Osoba WHERE idvoditelj = ${req.query.coachid}`, { type: QueryTypes.SELECT });
   
          if(voditelj.length != 0){

               // Get last 10 terms coached by coach ^^^
               var termini = await sequelize.query(
               `SELECT idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') AS datum, 
               (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova,
               to_char(vrijemePocetak::time , 'HH24:MI') AS vrijemePocetak, to_char(vrijemeZavrsetak::time , 'HH24:MI') as vrijemeZavrsetak, nazivLokacija,
               nazivIntenzitet, nazivVrsta, maxBrojClanova FROM Termin NATURAL JOIN Intenzitet NATURAL JOIN Vrsta NATURAL JOIN Lokacija WHERE idVoditelj = ${req.query.coachid} ORDER BY idTermin DESC LIMIT 10`,
               { type: QueryTypes.SELECT });


               var programi = await sequelize.query(
               `SELECT idProgram, idVoditelj, nazivProgram, to_char(datumZavrsetka::DATE, 'dd.mm.yyyy.') as datumZavrsetka FROM Zavrsio NATURAL JOIN ProgramiOsposobljavanja WHERE idVoditelj = ${req.query.coachid}`,
               { type: QueryTypes.SELECT });

               // Render required page
               res.render('coachInfo', {title: 'BITFIT - Voditelji', adminflag: req.session.adminflag, programi: programi, active: "Voditelji", greska: req.app.locals.greska , voditelj: voditelj[0], termini: termini, success: req.app.locals.success, zaposlenik: req.session.imeprezime});
     
               // Delete status messages
               req.app.locals.success = null;
               req.app.locals.greska = null;
          }

          else{

               // Set error message
               req.app.locals.greska = `Ne postoji voditelj sa ID: ${req.query.coachid}`;
          
               // Redirect to all users page
               res.redirect('/users');
          }
     }
});

router.post('/removeCertificate', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          try{
               // Delete certificate for coach
               await sequelize.query(
                    `DELETE FROM zavrsio WHERE idVoditelj = ${req.body.coachid} AND idProgram = ${req.body.programid}`,
                    { type: QueryTypes.DELETE });

               req.app.locals.success = `Uspjesno obrisano ogranicenje (ID: ${req.body.programid}) za voditelja (ID: ${req.body.coachid}).`;
          
          }catch(err){

               console.log(err);
               req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`
          }

          // Render required page
          res.redirect(307, '/coaches/info?coachid=' + req.body.coachid);
     }
});


router.post('/removeCoach', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

	     // Deleting coach & person from database
	     await Voditelj.destroy({where: {idvoditelj: req.body.coachid}});
          await Osoba.destroy({where: {idosoba: req.body.personid}});

	     // Setting success message
	     req.app.locals.success = `Voditelj (ID: ${req.body.coachid}) uspješno obrisan iz baze podataka!`

	     // Logging into actions.txt
	     logger.actions.info(`${functions.dateTime} - Removed coach ${req.body.id}`);

	     // Render required page
          res.redirect('/coaches');
     }
});

// POST Request for promoting member to coach
router.post('/addFromExistingMembers', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          var kif = 0;
          req.body.zavrsenkif == "on" ? kif = 1 : kif = 0;

          // Destroy user & insert coach

          await Korisnik.destroy({where: {idosoba: req.body.personid} });
     
          await sequelize.query(
               `INSERT INTO Voditelj (idosoba, zavrsenkif) VALUES (${req.body.personid}, ${kif});`,
               { type: QueryTypes.INSERT });

     
          // Set success message
          req.app.locals.success = `Korisnik uspješno promoviran u voditelja.`;
     
	     // Render required page
          res.redirect('/coaches');
     }
});

router.post('/addNewCoach', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Validation DATA
	     let data = {
               email: req.body.email, 
               date_of_birth: req.body.date_of_birth, 
               oib: req.body.oib, 
               first_name: req.body.first_name, 
               last_name: req.body.last_name, 
               password: req.body.password
          };

          // Validation
	     var status = coachSchema.validate(data, {abortEarly: false});

          // If validation is not OK
	     if(status.error){

		     req.app.locals.greska = status.error.details[0].message;
               console.log(req.app.locals.greska);
               res.redirect('/coaches/insert');
          }

          // If validation is OK
          else{
          

               var kif = 0;
               req.body.zavrsenkif == "on" ? kif = 1 : kif = 0;

               // Check if EMAIL or OIB exists in database
               var persons = await sequelize.query(`SELECT * FROM Osoba WHERE oibOsoba = ${req.body.oib} OR emailOsoba = '${req.body.email}';`,
               { type: QueryTypes.SELECT });

               
               if(persons.length == 0){

                    try{

                         var hash = await bcrypt.hash(req.body.password, saltRounds);

                         // Inserting person into database
                         await sequelize.query(
                              `INSERT INTO Osoba (emailOsoba, imeOsoba, prezimeOsoba, oibOsoba, lozinkaOsoba, datumRodenjaOsoba) VALUES
                              ('${req.body.email}','${req.body.first_name}','${req.body.last_name}','${req.body.oib}','${hash}','${req.body.date_of_birth}');`,
                              { type: QueryTypes.INSERT });

                         // Get ID from ^^^ person
                         var query = await sequelize.query(`SELECT idOsoba FROM Osoba WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT});

                         // Inserting coach into database
                         await sequelize.query(
                              `INSERT INTO Voditelj (idosoba, zavrsenkif) VALUES (${query[0].idosoba}, ${kif});`,
                              { type: QueryTypes.INSERT });

                         // Set success message
                         req.app.locals.success = `Voditelj ${req.body.first_name} ${req.body.last_name} uspješno upisan u bazu podataka.`

                    }catch(err){ 

                         console.log(err); 
                         req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`;
                    }

               }

               else req.app.locals.greska = `Osoba sa ovim e-mailom ili OIB-om je već u bazi!`;

          
               // Render required page
               res.redirect('/coaches');
          }
     }
});


module.exports = router;