var express = require('express');
var session = require('express-session');

const { QueryTypes } = require('sequelize');

var router = express.Router();
var Joi = require('@hapi/joi');

const bcrypt = require('bcrypt');
const saltRounds = 10;

var functions = require('../extras/functions');
var logger = require('../extras/logger');
var settings = require('../extras/settings');

const {sequelize } = require('../extras/sequelize')

router.use(express.json());
router.use(express.urlencoded({extended: true}));

// VALIDATION Schema
const userSchema = Joi.object().keys({

    email: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).email().required()
	.messages(settings.VALIDATION.emailMessages),

    first_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.firstNameMessages),

	last_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.lastNameMessages),

     oib: Joi.string().length(11).pattern(/^[0-9]+$/).required()
     .messages(settings.VALIDATION.oibMessages),

     password: Joi.string().min(settings.RULES.minPasswordLength).max(settings.RULES.maxPasswordLength).required()
     .messages(settings.VALIDATION.passwordMessages),

     date_of_birth: Joi.string().pattern(/^((0[1-9])|(1[\d])|(2[\d])|(3[01]))[.]((0[1-9])|(1[12]))[.]([\d]{4})[.]$/).required()
     .messages(settings.VALIDATION.dateOfBirthMessages)
});


// Post for registration
router.post('/register', async function (req, res, next) {

        console.log(req.body.email);

          // Validation DATA
	     let data = {

               password: req.body.password,
               email: req.body.email, 
               date_of_birth: req.body.date_of_birth, 
               oib: req.body.oib, 
               first_name: req.body.first_name, 
               last_name: req.body.last_name
          };

          // Validation
	     var status = userSchema.validate(data, {abortEarly: false});

          // If validation is not OK
	     if(status.error){
		    res.json({success: 0, msg: status.error.details[0].message});
            return next();

         }

          // If validation is OK
          else{
            console.log(req.body.email);
            // Check if EMAIL or OIB exists in database
            var persons = await sequelize.query(`SELECT * FROM Osoba WHERE oibOsoba = ${req.body.oib} OR emailOsoba = '${req.body.email}';`,
            { type: QueryTypes.SELECT });

            if(persons.length != 0){
                res.json({success: 0, msg: settings.URS_EXISTS_MSG});
                return next();
            }

               
            if(persons.length == 0){
	          try{

                    var hash = await bcrypt.hash(req.body.password, saltRounds);
    
                    
                    // Inserting person into database
                    await sequelize.query(
                    `INSERT INTO Osoba (emailOsoba, imeOsoba, prezimeOsoba, oibOsoba, lozinkaOsoba, datumRodenjaOsoba) VALUES
                    ('${req.body.email}','${req.body.first_name}','${req.body.last_name}','${req.body.oib}','${hash}','${req.body.date_of_birth}');`,
                    { type: QueryTypes.INSERT });


                    // Get ID from person (FK for User Column)
                    var query = await sequelize.query(`SELECT idOsoba FROM Osoba WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT});
     
                    // Inserting user into database
                    await sequelize.query(
                    `INSERT INTO Korisnik (idosoba) VALUES (${query[0].idosoba});`,
                    { type: QueryTypes.INSERT });

                     res.json({success: 1, msg: settings.REG_SUCC_MSG});

                    // Logging into actions.txt
	                logger.actions.info(`${functions.dateTime} - New user registration - ${req.body.email}`);
                    
               

               }catch(err){ 
                    console.log(err); 
                    res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
                    return next();
               }

               }
          }
     
});


// POST Request for login
router.post('/login', async function(req, res, next){


	
    // Default data
    var acc = {
        id: null,
        type: null,
    }

    if(req.body.email == null || req.body.password == null){

        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, acc: acc});
        return next();
    }


    try{

    // Search for data in users
    var user = await sequelize.query(`
    SELECT idKorisnik, emailOsoba, lozinkaOsoba 
    FROM Korisnik NATURAL JOIN Osoba
    WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT});

    
    // If user is found, get ID and set account type to "user"
    if(user.length != 0){
        if(await bcrypt.compare(req.body.password, user[0].lozinkaosoba)){
            acc = {id: user[0].idkorisnik, type: "user"};
            res.json({success: 1, msg: settings.LOGIN_SUCC_MSG, acc: acc});
            return next();
        }
    }

    // If user is not found, search for data in coaches  
    else{

        var coach = await sequelize.query(`
            SELECT idVoditelj, emailOsoba, lozinkaOsoba 
            FROM Voditelj NATURAL JOIN Osoba
            WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT });

        // If coach is found, get ID and set account type to "coach"
        if(coach.length != 0)
            if(await bcrypt.compare(req.body.password, coach[0].lozinkaosoba)){
                acc = {id: coach[0].idvoditelj, type: "coach"};
                res.json({success: 1, msg: settings.LOGIN_SUCC_MSG, acc: acc});
                return next();
            }
    }

    }catch(err){
        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, acc: acc});
        return next();

    }

    res.json({success: 0, msg: settings.LOGIN_ERR_MSG, acc: acc});
});


// GET Request for all locations
router.get('/locations', async function(req, res, next) {

    try{

        var locations = await sequelize.query(
        `SELECT * 
        FROM Lokacija NATURAL JOIN SeNalaziU
        NATURAL JOIN Mjesto`, { type: QueryTypes.SELECT });
   
      
        res.json({success: 1, locations: locations});
    }catch(err){

        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, locations: null});
    }

});


// GET Request for all locations
router.get('/termTypes', async function(req, res, next) {

    try{
        
        var types = await sequelize.query(
        `SELECT idVrsta, UPPER(nazivVrsta) as nazivVrsta, opisVrsta FROM Vrsta`, { type: QueryTypes.SELECT });
        
        res.json({success: 1, types: types});

    }catch(err){

        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, types: null});
    }

});


// GET Data for creating new term
router.get('/termIntensities', async function(req, res, next) {


    try{

        var intensities = await sequelize.query(
        `SELECT idIntenzitet, UPPER(nazivIntenzitet) as nazivIntenzitet, opisIntenzitet FROM Intenzitet`, { type: QueryTypes.SELECT });

        

        res.json({success: 1, intensities: intensities});

    }catch(err){

        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, intensities: null});
    }
});


// GET Data for terms coached by specified coach
router.get('/termsForCoach', async function(req, res, next) {


    try{


         var upcomingTerms = await sequelize.query(
        `SELECT 
        nazivLokacija, idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') as datumTermin,
        to_char(vrijemePocetak::TIMESTAMP(0), 'HH24:MI') as vrijemePocetak,
        to_char(vrijemeZavrsetak::TIMESTAMP(0), 'HH24:MI') as vrijemeZavrsetak,
        nazivIntenzitet, nazivVrsta, idIntenzitet, maxBrojClanova,
        (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova
        FROM Termin  NATURAL JOIN Vrsta NATURAL JOIN Intenzitet NATURAL JOIN Lokacija
        WHERE idVoditelj = ${req.query.coachid} AND vrijemePocetak > (NOW() + '2 hours');`, {type: QueryTypes.SELECT});


        var activeOrFinishedTerms = await sequelize.query(
        `SELECT 
        nazivLokacija, idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') as datumTermin,
        to_char(vrijemePocetak::TIMESTAMP(0), 'HH24:MI') as vrijemePocetak,
        to_char(vrijemeZavrsetak::TIMESTAMP(0), 'HH24:MI') as vrijemeZavrsetak,
        nazivIntenzitet, nazivVrsta, idIntenzitet, maxBrojClanova,
        (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova
        FROM Termin NATURAL JOIN Vrsta NATURAL JOIN Intenzitet NATURAL JOIN Lokacija
        WHERE idVoditelj = ${req.query.coachid} AND vrijemePocetak < (NOW() + '2 hours');`, {type: QueryTypes.SELECT});


        res.json({success: 1, activeOrFinishedTerms: activeOrFinishedTerms, upcomingTerms: upcomingTerms});

    }catch(err){

        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, activeOrFinishedTerms: null, upcomingTerms: null});
    }
});

// GET Data for terms coached by specified coach
router.get('/usersOnTerm', async function(req, res, next) {


    try{


        var users = await sequelize.query(
        `SELECT 
        idKorisnik, imeOsoba, prezimeOsoba,
        to_char(datumRodenjaOsoba, 'dd.mm.yyyy.') as datumRodenjaOsoba,
        oibOsoba, emailOsoba, statusDolaska
        FROM Rezervira NATURAL JOIN Korisnik NATURAL JOIN Osoba
        WHERE idTermin = ${req.query.termid}`, {type: QueryTypes.SELECT});


        res.json({success: 1, users: users});

    }catch(err){

        console.log(err);
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG, users: null});
    }
});

// GET Request for terms on specified location
router.get('/termsOnLocation', async function(req, res, next){


    console.log(req.query.locationid);

    if(req.query.locationid == null){
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG});
        return next();
    }
    
    // If everything is ok, proceed
    else{

        try{
        var terms = await sequelize.query(
        `SELECT 
        nazivLokacija, idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') as datumTermin,
        to_char(vrijemePocetak::TIMESTAMP(0), 'HH24:MI') as vrijemePocetak,
        to_char(vrijemeZavrsetak::TIMESTAMP(0), 'HH24:MI') as vrijemeZavrsetak,
        idVoditelj, imeOsoba, prezimeOsoba,
        nazivIntenzitet, nazivVrsta, idIntenzitet, maxBrojClanova,
        (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova
        FROM Termin NATURAL JOIN Voditelj NATURAL JOIN Osoba NATURAL JOIN Vrsta NATURAL JOIN Intenzitet NATURAL JOIN Lokacija
        WHERE idLokacija = ${req.query.locationid} AND vrijemePocetak > (NOW() + '2 hours');`, {type: QueryTypes.SELECT});


            for(var term of terms){
               
                var restrictions = await sequelize.query(
                `SELECT nazivOgranicenje FROM OgranicenjaZaTrening NATURAL JOIN Ogranicenje WHERE idTermin = ${term.idtermin}`, {type: QueryTypes.SELECT});
        
                term['restrictions'] = restrictions;                
            }

        console.log(JSON.stringify(terms, null, 2));

        res.json({success: 1, terms: terms});
        return next();
        }catch(err){

            console.log(err);
            res.json({success: 0, msg: settings.DEFAULT_ERR_MSG});
            return next();
        }
    }
});

// GET Request for profile info
router.get('/userInfo', async function(req, res, next){

    // Check if all parameters are received
    if(req.query.type == null || req.query.id == null){
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG});
        return next();
    }

    // If account type is user, get info for user
    else if(req.query.type == "user"){
        

// Get all users & their membership expiration
        
        var info = await sequelize.query(
        `SELECT 
        Korisnik.idKorisnik, imeOsoba, to_char(MAX(vrijemeIstekaClanarina)::DATE, 'dd.mm.yyyy.') as istekClanarina,
        prezimeOsoba, to_char(datumRodenjaOsoba::DATE,'dd.mm.yyyy.') as datumRodenjaOsoba,
        oibOsoba, emailOsoba,
		(SELECT COUNT(*) FROM Rezervira WHERE idKorisnik = ${req.query.id} AND statusDolaska = 1) as brOdradenih, (SELECT COUNT(*) FROM Rezervira WHERE idKorisnik = ${req.query.id}) as brTreninga
        FROM Korisnik NATURAL JOIN Osoba LEFT JOIN Clanarina on Korisnik.idKorisnik = Clanarina.idKorisnik
        WHERE Korisnik.idKorisnik = ${req.query.id}
        GROUP BY Korisnik.idKorisnik, imeOsoba, prezimeOsoba, datumRodenjaOsoba, oibOsoba, emailOsoba;`, {type: QueryTypes.SELECT});     

        var term = await sequelize.query(
        `SELECT 	idTermin, to_Char(vrijemePocetak::DATE, 'dd.mm.yyyy.') as datumTermin,
		to_char(vrijemePocetak::TIMESTAMP, 'HH24:MI') as vrijemePocetak,
		to_char(vrijemeZavrsetak::TIMESTAMP, 'HH24:MI') as vrijemeZavrsetak,
		nazivVrsta, idIntenzitet, imeosoba,  prezimeosoba,
		((SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) 
		 || '/' || maxBrojClanova) as brojRezerviranih
        FROM Termin NATURAL JOIN Lokacija NATURAL JOIN Vrsta NATURAL JOIN Rezervira NATURAL JOIN Voditelj NATURAL JOIN Osoba
        WHERE idKorisnik = ${req.query.id} AND NOW() + '2 hours' < vrijemeZavrsetak
        ORDER BY idTermin ASC LIMIT 1`, { type: QueryTypes.SELECT });

    }

    // If account type is coach, get info for coach
    else if(req.query.type == "coach"){

        var info = await sequelize.query(
        `SELECT 
        idVoditelj, imeOsoba, 
        prezimeOsoba, to_char(datumRodenjaOsoba::DATE,'dd.mm.yyyy.') as datumRodenjaOsoba,
        oibOsoba, emailOsoba, zavrsenKif,
		(SELECT COUNT(*) FROM Termin WHERE idVoditelj = ${req.query.id}) as brTreninga,
		(SELECT COUNT(idKorisnik) FROM Rezervira NATURAL JOIN Termin WHERE idVoditelj = ${req.query.id} AND statusDolaska = 1) as ukDolaznost
        FROM Voditelj NATURAL JOIN Osoba
        WHERE idVoditelj = ${req.query.id}`, {type: QueryTypes.SELECT});  

        // TODO: Get upcoming term
        var term = await sequelize.query(
        `SELECT idTermin, to_Char(vrijemePocetak::DATE, 'dd.mm.yyyy.') as datumTermin,
		to_char(vrijemePocetak::TIMESTAMP, 'HH24:MI') as vrijemePocetak,
		to_char(vrijemeZavrsetak::TIMESTAMP, 'HH24:MI') as vrijemeZavrsetak,
		nazivVrsta, idIntenzitet, nazivLokacija,
		((SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) 
		|| '/' || maxBrojClanova) as brojRezerviranih
        FROM Termin NATURAL JOIN Lokacija NATURAL JOIN Vrsta
        WHERE idVoditelj = ${req.query.id} AND NOW() + '2 hours' < vrijemePocetak
        ORDER BY idTermin ASC LIMIT 1`, { type: QueryTypes.SELECT });

    }

    else{

        var info = null;
        var term = null;
    }

    var success = 0;
    if(info != null) success = 1;

    res.json({success: success, info: info[0], term: term[0]});
});

router.post('/createTerm', async function(req, res, next){

    // Check for blank values
    if(req.body.termstart == null || req.body.termend == null || req.body.termdate == null
    || req.body.type == null || req.body.intensity == null || req.body.locationid == null || req.body.maxusers == null || req.body.maxusers == ""){
        
        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG});
        return next();
    }

    console.log(req.body.restrictions);
  
    // Check if maxUsers number is between constraints
    if(parseInt(req.body.maxusers) < 1 || parseInt(req.body.maxusers) > 50){

        res.json({success: 0, msg: "Broj korisnika mora biti između 1 i 50!"});
        return next();
    }
    

    try{

        var start = req.body.termdate.split(" ")[0] + " " + req.body.termstart;
        var end = req.body.termdate.split(" ")[0] + " " + req.body.termend;
        

        // Check if DATE has not passed already
        var termDate = new Date(req.body.termdate);
        var currentDate = new Date();
        currentDate.setHours(2,0,0,0);

        // if termDate < today's Date , NOT OK
        if(termDate < currentDate){

            res.json({success: 0, msg: "Odabrali ste nevažeći datum!"});
            return next();
        }

        var time1split = req.body.termstart.split(':');
        var time2split = req.body.termend.split(':');

        // If termStart < termEnd, OK
        if(time1split[0] < time2split[0] || (time1split[0] == time2split[0]
        && time1split[1] < time2split[1])){   


        // Check if coach has active term in that time range
        var termId = await sequelize.query(
        `SELECT idTermin FROM Termin WHERE idVoditelj = ${req.body.coachid}
        AND (vrijemePocetak BETWEEN '${start}' AND '${end}' OR vrijemeZavrsetak BETWEEN '${start}' AND '${end}');`,
        {type: QueryTypes.SELECT });

        if(termId.length != 0){

            res.json({success: 0, msg: "Imate aktivan termin u tom periodu!"});
            return next();

        }

            console.log(start);
            console.log(end);

            await sequelize.query(
            `INSERT INTO Termin(idVoditelj, idLokacija, idIntenzitet, idVrsta, vrijemePocetak, vrijemeZavrsetak, maxBrojClanova) VALUES
            (${req.body.coachid},${req.body.locationid},${req.body.intensity},${req.body.type}, '${start}', '${end}', ${req.body.maxusers});`, {type: QueryTypes.SELECT });
        
            var termId = await sequelize.query(
                         `SELECT idTermin FROM Termin WHERE idVoditelj = ${req.body.coachid} AND idLokacija = ${req.body.locationid} 
                         AND (vrijemePocetak = '${start}' AND vrijemeZavrsetak = '${end}');`,
                         {type: QueryTypes.SELECT });

            console.log("Tu sam");
            for(var restriction of req.body.restrictions){
            
                var id = await sequelize.query(`SELECT idOgranicenje FROM Ogranicenje WHERE nazivOgranicenje = '${restriction}'`, {type: QueryTypes.SELECT });
                console.log(id);
                await sequelize.query(`INSERT INTO OgranicenjaZaTrening (idTermin, idOgranicenje) VALUES (${termId[0].idtermin},${id[0].idogranicenje})`, {type: QueryTypes.INSERT});
            }


            res.json({success: 1, msg: "Termin uspješno kreiran"});
            return next();
        }

        else{

            res.json({success: 0, msg: "Unijeli ste nevažeće vrijeme!"});
            return next();


        }
    }catch(err){

        console.log(err);
        res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
    }

});

// POST for reservation of term
router.post('/reserveTerm', async function(req, res, next){

    // Check if all parameters are received
    if(req.body.type == null || req.body.id == null || req.body.termid == null){

        res.json({success: 0, msg: settings.DEFAULT_ERR_MSG});
        return next();
    }

    // Check if account type is user & id not null
    else if(req.body.type != "user" || req.body.id == null){

        res.json({success: 0, msg: "Niste korisnik!"});
        return next();
    }

    // If everything is ok, proceed
    else{

        // Check if user has active membership and doesn't have 3 missed reserved terms
        var memberships = await sequelize.query(
        `SELECT * FROM Korisnik
        NATURAL JOIN Clanarina
        WHERE idKorisnik = ${req.body.id}
        AND NOW() BETWEEN vrijemePlacanjaClanarina::TIMESTAMP AND vrijemeIstekaClanarina::TIMESTAMP
        AND propusteniDolasci < 3`, { type: QueryTypes.SELECT}); 


        if(memberships.length == 0){

            res.json({success: 0, msg: "Nemate aktivnu članarinu."});
            return next();
        }
    
        // Check if term has already started
        var term = await sequelize.query(`SELECT * FROM Termin WHERE idTermin = ${req.body.termid} AND NOW() < vrijemePocetak`, {type: QueryTypes.SELECT });

        if(term.length == 0){

            res.json({success: 0, msg: "Ovaj termin više nije moguće rezervirati."});
            return next();
        }

        // Check  if user already has reserved terms that haven't finished
        var active = await sequelize.query(
        `SELECT * 
        FROM Rezervira NATURAL JOIN Termin
        WHERE vrijemeZavrsetak > NOW()
        AND idKorisnik = ${req.body.id};`, {type: QueryTypes.SELECT});

        if(active.length != 0){

            res.json({success: 0, msg: "Već imate aktivan termin."});
            return next();
        }

        // If everything is ok, proceed
        try{

            await sequelize.query(
            `INSERT INTO Rezervira
            (idKorisnik, idTermin,
            vrijemeRezervacija, statusDolaska) VALUES
            (${req.body.id}, ${req.body.termid}, NOW(), 0)`, {type: QueryTypes.INSERT});

            res.json({success: 1, msg: "Termin uspješno rezerviran!"});

        }catch(err){

            console.log(err);
            res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
        }
    }
});

// POST for cancelling term
router.post('/setArrival', async function(req, res, next){
    console.log(req.body.userid + " " + req.body.termid + " " + req.body.status);
    if(req.body.userid == null || req.body.termid == null || req.body.status == null){

        res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
        console.log("Test");
        return next();
        
    }
    
    var terms = await sequelize.query(`SELECT * FROM Rezervira WHERE idTermin = ${req.body.termid} AND idKorisnik = ${req.body.userid}`, {type: QueryTypes.SELECT });
    console.log(terms.length);
    if(terms.length == 0){
        console.log("Test 3");
        res.json({success: 0,  msg: "Korisnik nije rezervirao navedeni termin!"});
        return next();

    }
    
    else{
        
        await sequelize.query(`UPDATE Rezervira SET statusDolaska = ${req.body.status} WHERE idTermin = ${req.body.termid} AND idKorisnik = ${req.body.userid}`, {type: QueryTypes.UPDATE });
        console.log("Success");
        res.json({success: 1});
        
    }
});

// POST for cancelling term
router.post('/cancelTerm', async function(req, res, next){

    // Check if all parameters are received
    if(req.body.type == null || req.body.id == null || req.body.termid == null){

        res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
        return next();
    }

    // Check if account type is user & id not null
    else if(req.body.type != "user" || req.body.id == null){

        res.json({success: 0, msg: "Niste korisnik!"});
        return next();
    }

    // If everything is ok, proceed
    else{

        // Check if reservation exists
         var reservation = await sequelize.query(`SELECT * FROM Rezervira WHERE idTermin = ${req.body.termid} AND idKorisnik = ${req.body.id}`, {type: QueryTypes.SELECT });

        if(reservation.length == 0){

            res.json({success: 0, msg: "Ne postoji rezervacija za ovaj termin!"});
            return next();
        }
    
        // Check if term has already started
        var term = await sequelize.query(`SELECT * FROM Termin WHERE idTermin = ${req.body.termid} AND NOW() < vrijemePocetak`, {type: QueryTypes.SELECT });

        if(term.length == 0){

            res.json({success: 0, msg: "Ovaj termin više nije moguće otkazati."});
            return next();
        }

        // If everything is ok, proceed
        try{

            await sequelize.query(
            `DELETE FROM Rezervira
            WHERE
            idkorisnik = ${req.body.id} AND idtermin = ${req.body.termid};`, {type: QueryTypes.INSERT});

            res.json({success: 1, msg: "Termin uspješno otkazan."});

        }catch(err){

            console.log(err);
            res.json({success: 0,  msg: settings.DEFAULT_ERR_MSG});
        }
    }

});

module.exports = router;