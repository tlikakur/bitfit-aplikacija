var express = require('express');
var session = require('express-session');


const { QueryTypes } = require('sequelize');

var router = express.Router();
var Joi = require('@hapi/joi');

var functions = require('../extras/functions');
var logger = require('../extras/logger');
var settings = require('../extras/settings');

const {Ogranicenje, sequelize } = require('../extras/sequelize')

router.use(express.json());
router.use(express.urlencoded({extended: true}));


// VALIDATION schema
const restrictionSchema = Joi.object().keys({

    restriction_name: Joi.string().min(settings.RULES.minRestrictionLength).max(settings.RULES.maxRestrictionLength).required()
	.messages(settings.VALIDATION.restrictionMessages),
    
});

// GET Request for displaying all extras
router.get('/', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);
    else if(!req.session.adminflag){

        req.app.locals.greska = `Za ovu akciju potrebne su administratorske ovlasti!`;
        res.redirect('/users');

     }

    else{


        var reservationsbymonth = await sequelize.query(
        `SELECT
        EXTRACT(MONTH FROM Termin.vrijemePocetak) AS  terminMjesec,
        COUNT(idkorisnik) AS countKorisnik,
	    COUNT(DISTINCT Termin.idtermin) as countTermin
	   
        FROM Rezervira RIGHT JOIN Termin on Rezervira.idTermin = Termin.idTermin
        WHERE (TERMIN.vrijemePocetak >= DATE_TRUNC('year', NOW()))
        GROUP BY EXTRACT(MONTH FROM Termin.vrijemePocetak);`,
        {type: QueryTypes.SELECT});

        var direction = "DESC";
        if(req.query.statsDir == 1) direction = "ASC";

        var mostactivecoaches = await sequelize.query(

        `SELECT
        idVoditelj, imeOsoba, prezimeOsoba,
        (SELECT COUNT(*) FROM Termin WHERE Termin.idVoditelj = Voditelj.idVoditelj) as brTreninga,

        round(
	        (SELECT COUNT(*) FROM Rezervira NATURAL JOIN Termin WHERE Termin.idVoditelj = Voditelj.idVoditelj)::NUMERIC /
	        (SELECT COUNT(*) FROM Termin WHERE Termin.idVoditelj = Voditelj.idVoditelj)::NUMERIC, 2
        ) as prosjDolaznost

        FROM Voditelj NATURAL JOIN Osoba
        ORDER BY prosjDolaznost ${direction}, brTreninga ${direction} LIMIT 3`, {type: QueryTypes.SELECT});

        var mostactiveusers = await sequelize.query(
        `SELECT
        idKorisnik, imeOsoba, prezimeOsoba,
        (SELECT COUNT(*) FROM Rezervira WHERE Rezervira.idKorisnik = Korisnik.idKorisnik) as brRezerviranih,
        (SELECT COUNT(*) FROM Rezervira WHERE Rezervira.idKorisnik = Korisnik.idKorisnik AND statusDolaska = 1) as brOdradenih

        FROM Korisnik NATURAL JOIN Osoba
        ORDER BY brRezerviranih DESC, brOdradenih DESC LIMIT 3`, {type: QueryTypes.SELECT});

        var usersstats = await sequelize.query(
        `SELECT COUNT(*) as ukKorisnika, SUM(cijenaClanarina) as ukClanarina FROM Korisnik NATURAL JOIN Clanarina`,
        {type: QueryTypes.SELECT});

        var coachstats = await sequelize.query(
        `SELECT COUNT( DISTINCT Voditelj.idVoditelj) as ukVoditelja, 
        COUNT(*) AS ukTermina 
        FROM Termin LEFT JOIN Voditelj ON Termin.idVoditelj = Voditelj.idVoditelj`,
        {type: QueryTypes.SELECT});

        var reservationsdata = [];
        var termsdata = [];
     
        for(var i=1; i<=12; i++){

            reservationsdata.push(0);
            termsdata.push(0);
        }

        for(var item of reservationsbymonth){

            reservationsdata[item['terminmjesec']-1] = parseInt(item['countkorisnik']);
            termsdata[item['terminmjesec']-1] = parseInt(item['countkorisnik']);

        }

        


        // Display required page
        res.render('stats', {title: 'BITFIT - Statistika', active: "Statistika", adminflag: req.session.adminflag,
                            greska: req.app.locals.greska, success: req.app.locals.success,
                            zaposlenik: req.session.imeprezime, mostactivecoaches: mostactivecoaches,
                            mostactiveusers: mostactiveusers, usersstats: usersstats[0], coachstats: coachstats[0],
                            reservationsdata: reservationsdata, termsdata: termsdata, direction: direction});

         // Reset error & success messages after displaying
        req.app.locals.success = null;
        req.app.locals.greska = null;
     }
});





module.exports = router;