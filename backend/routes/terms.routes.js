var express = require('express');
var session = require('express-session');
const Joi = require('@hapi/joi');

const { QueryTypes } = require('sequelize');

var router = express.Router();
const settings = require('../extras/settings');
var functions = require('../extras/functions');
var logger = require('../extras/logger');
const { Korisnik, Osoba, Clanarina, sequelize } = require('../extras/sequelize')

router.use(express.json());
router.use(express.urlencoded({extended: true}));


// VALIDATION Schema
const termSchema = Joi.object().keys({

    idvoditelj: Joi.number().integer().required()
	.messages(settings.VALIDATION.coachMessages),

    idlokacija: Joi.number().integer().required()
	.messages(settings.VALIDATION.locationMessages),

	maxclanova: Joi.number().integer().min(settings.RULES.minUsers).max(settings.RULES.maxUsers).required()
	.messages(settings.VALIDATION.maxUsersMessages),

     vrijemepocetak: Joi.string().required()
     .messages(settings.VALIDATION.termStartMessages),

     vrijemezavrsetak: Joi.string().required()
     .messages(settings.VALIDATION.termEndMessages),

     datum: Joi.string().required()
     .messages(settings.VALIDATION.termDateMessages)
});


// GET (Unfinished) Terms
router.get('/', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          
          // Get all terms query
          var terms = await sequelize.query(`SELECT 
               idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy') AS datum, to_char(vrijemePocetak::time , 'HH24:MI') AS vrijemePocetak, to_char(vrijemeZavrsetak::time , 'HH24:MI') as vrijemeZavrsetak, maxBrojClanova,
               imeOsoba, prezimeOsoba, nazivLokacija, nazivIntenzitet, nazivVrsta, NOW() + '2 hours',
               (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova
               FROM Termin NATURAL JOIN Intenzitet NATURAL JOIN Vrsta
               NATURAL JOIN Lokacija NATURAL JOIN Voditelj NATURAL JOIN Osoba
               WHERE Termin.vrijemeZavrsetak > NOW() + '2 hours'`,
               { type: QueryTypes.SELECT });


          // Display required page
          res.render('terms', {heading: "AKTIVNIH TERMINA", title: 'BITFIT - Termini', active: "Termini", adminflag: req.session.adminflag, greska: req.app.locals.greska, success: req.app.locals.success, termini: terms, zaposlenik: req.session.imeprezime});

          // Reset error & success messages after displaying
          req.app.locals.success = null;
          req.app.locals.greska = null;
     }
});


// GET Insert page
router.get('/insert', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get all needed data
          var intenziteti = await sequelize.query(`SELECT * FROM Intenzitet`, { type: QueryTypes.SELECT });
          var vrstetreninga = await sequelize.query(`SELECT * FROM Vrsta`, { type: QueryTypes.SELECT });
          var voditelji = await sequelize.query(`SELECT * FROM Voditelj NATURAL JOIN Osoba`, { type: QueryTypes.SELECT });
          var lokacije = await sequelize.query(`SELECT * FROM Lokacija`, { type: QueryTypes.SELECT });
          var ogranicenja = await sequelize.query(`SELECT * FROM Ogranicenje`, { type: QueryTypes.SELECT });

          // Display required page
          res.render('termInsert', {title: 'BITFIT - Termini', ogranicenja: ogranicenja, adminflag: req.session.adminflag, active: "Termini", greska: req.app.locals.greska, success: null, voditelji: voditelji, intenziteti: intenziteti, vrstetreninga: vrstetreninga, lokacije: lokacije, zaposlenik: req.session.imeprezime});

          // Reset error message
          req.app.locals.greska = null;

      }
});

// POST from Insert page
router.post('/insert', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

           // Validation DATA
	     let data = {
               idvoditelj: req.body.idvoditelj, 
               idlokacija: req.body.idlokacija, 
               maxclanova: req.body.maxcl, 
               vrijemepocetak: req.body.termstart, 
               vrijemezavrsetak: req.body.termend, 
               datum: req.body.termdate
          };

          // Validation
	     var status = termSchema.validate(data, {abortEarly: false});


          // If validation is not OK
	     if(status.error){

		     req.app.locals.greska = status.error.details[0].message;
               console.log(req.app.locals.greska);
               res.redirect('/terms/insert');
          }

          else{


               var termStart = req.body.termdate + " " + req.body.termstart;
               var termEnd = req.body.termdate + " " + req.body.termend;

               // Check if DATE has not passed already
               var termDate = new Date(req.body.termdate);
               var  currentDate = new Date();
               currentDate.setHours(2,0,0,0);

               
               // if termDate < today's Date , NOT OK
               if(termDate < currentDate){

                    req.app.locals.greska = `Nije moguće odabrati datum koji je prošao!`
                    res.redirect('/terms/insert');
               }

                // if termDate >= today's Date , OK
               else{

                    var time1split = req.body.termstart.split(':');
                    var time2split = req.body.termend.split(':');

                    // If termStart < termEnd, OK
                    if(time1split[0] < time2split[0] || (time1split[0] == time2split[0]
                    && time1split[1] < time2split[1])){   

                         // TODO: Check if coach already has active term in that time range

                     
                         var termId = await sequelize.query(
                         `SELECT idTermin FROM Termin WHERE idVoditelj = ${req.body.idvoditelj}
                         AND (vrijemePocetak BETWEEN '${termStart}' AND '${termEnd}' OR vrijemeZavrsetak BETWEEN '${termStart}' AND '${termEnd}');`,
                         {type: QueryTypes.SELECT });

                         if(termId.length == 0){

                              try{

                                   // Insert term query
                                   await sequelize.query(
                                        `INSERT INTO Termin
                                        (vrijemePocetak, vrijemeZavrsetak, maxBrojClanova, idVoditelj, idLokacija, idIntenzitet, idVrsta) VALUES
                                        ('${termStart}', '${termEnd}', ${req.body.maxcl}, ${req.body.idvoditelj}, ${req.body.idlokacija}, ${req.body.idintenzitet}, ${req.body.idvrsta});`,
                                        { type: QueryTypes.INSERT });

                                   // Get ID of inserted term
                                   var termId = await sequelize.query(
                                        `SELECT idtermin FROM Termin WHERE vrijemePocetak = '${termStart}' AND vrijemeZavrsetak = '${termEnd}' AND idLokacija = ${req.body.idlokacija} AND idVoditelj = ${req.body.idvoditelj}`,
                                        {type: QueryTypes.SELECT });

                                   // For all ticked restrictions, insert pair (termID, restrictionID)
                                   console.log(req.body.ogranicenja);
                                   if(req.body.ogranicenja!= [] && req.body.ogranicenja != null) for(var idogranicenje of req.body.ogranicenja){

                                        // Insert term query
                                        await sequelize.query(
                                             `INSERT INTO ogranicenjaZaTrening (idOgranicenje, idTermin) VALUES
                                             (${idogranicenje}, ${termId[0].idtermin});`,
                                             { type: QueryTypes.INSERT });
                                   }

                                   // Set success message
                                   req.app.locals.success = `Termin uspješno dodan u bazu podataka.`

                              }catch(err){

                                   console.log(err);
                                   req.app.locals.greska = `Dogodila se greška.`
                              }

                         } else
                              req.app.locals.greska = `Odabrani voditelj već ima zakazan termin koji se preklapa sa odabranim terminom!`

                         // Display required page     
                         res.redirect('/terms');
                    }

                    // If termStart >= termEnd, NOT OK
                    else{
                         req.app.locals.greska = `Vrijeme početka treninga ne može biti veće od vremena završetka treninga!`

                         // Display required page     
                         res.redirect('/terms/insert')
                    }
               }
          }
     }
});

// GET* Info about term
router.post('/info', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get term info
          var term = await sequelize.query(`SELECT idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy') AS datum,
          to_char(vrijemePocetak::time , 'HH24:MI') AS vrijemePocetak, to_char(vrijemeZavrsetak::time , 'HH24:MI') as vrijemeZavrsetak, maxBrojClanova,
          imeOsoba, prezimeOsoba, nazivLokacija, nazivIntenzitet, nazivVrsta
           FROM Termin NATURAL JOIN Voditelj NATURAL JOIN Osoba NATURAL JOIN Intenzitet NATURAL JOIN Vrsta NATURAL JOIN Lokacija WHERE idTermin=${req.body.termid}`,
          { type: QueryTypes.SELECT });

          // Get users from term ^^^
          var korisnici = await sequelize.query(`SELECT idKorisnik, imeOsoba, prezimeOsoba, to_char(vrijemeRezervacija, 'dd.mm.yyyy. HH:MM') as vrijemeRezervacija, statusDolaska
          FROM Rezervira NATURAL JOIN Korisnik NATURAL JOIN Osoba
          WHERE idTermin = ${req.body.termid};`,
          { type: QueryTypes.SELECT });

          // Get restrictions from term ^^^
          var ogranicenja = await sequelize.query(` SELECT nazivOgranicenje from OgranicenjaZaTrening NATURAL JOIN Ogranicenje WHERE idTermin = ${req.body.termid}`,
          {type: QueryTypes.SELECT });

          // Display required page
          res.render('termInfo', {title: 'BITFIT - Termini', active: "Termini", ogranicenja: ogranicenja, adminflag: req.session.adminflag, greska: null, success: null, term: term[0], korisnici: korisnici, zaposlenik: req.session.imeprezime});
     }
});

// Get all finished terms
router.get('/finished', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get all finished terms query
          var terms = await sequelize.query(`SELECT 
               idTermin, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') AS datum, to_char(vrijemePocetak::time , 'HH24:MI') AS vrijemePocetak, to_char(vrijemeZavrsetak::time , 'HH24:MI') as vrijemeZavrsetak, maxBrojClanova,
               imeOsoba, prezimeOsoba, nazivLokacija, nazivIntenzitet, nazivVrsta, NOW()+'2 hours',
               (SELECT COUNT(idKorisnik) FROM rezervira WHERE rezervira.idTermin = Termin.idTermin) as brojClanova
               FROM Termin NATURAL JOIN Intenzitet NATURAL JOIN Vrsta 
               NATURAL JOIN Lokacija NATURAL JOIN Voditelj NATURAL JOIN Osoba
                WHERE Termin.vrijemeZavrsetak < NOW()+'2 hours'`,
               { type: QueryTypes.SELECT });

          // Display required page
          res.render('terms', {heading: "ZAVRŠENIH TERMINA", title: 'BITFIT - Termini', active: "Termini", adminflag: req.session.adminflag, greska: null, success: null, termini: terms, zaposlenik: req.session.imeprezime});
     }
});


// POST to remove term with specified termid
router.post('/removeTerm', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          try{

               // Delete specified term
               await sequelize.query(`DELETE FROM Termin WHERE idTermin = ${req.body.termid}`, { type: QueryTypes.DELETE });

               // Set success message
               req.app.locals.success = `Termin uspješno obrisan iz baze podataka.`

          }catch(err){

               console.log(err);
               req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`;
          }
     
          // Redirect
          res.redirect('/terms');
     }
});




module.exports = router;