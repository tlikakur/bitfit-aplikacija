var express = require('express');
var session = require('express-session');


const { QueryTypes } = require('sequelize');

var router = express.Router();
var Joi = require('@hapi/joi');

var functions = require('../extras/functions');
var logger = require('../extras/logger');
var settings = require('../extras/settings');

const {Ogranicenje, sequelize } = require('../extras/sequelize')

router.use(express.json());
router.use(express.urlencoded({extended: true}));


// VALIDATION schema
const restrictionSchema = Joi.object().keys({

    restriction_name: Joi.string().min(settings.RULES.minRestrictionLength).max(settings.RULES.maxRestrictionLength).required()
	.messages(settings.VALIDATION.restrictionMessages),
    
});

// GET Request for displaying all extras
router.get('/', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

    else{

        // Get locations from database
        var lokacije = await sequelize.query(`SELECT * FROM Lokacija  NATURAL JOIN seNalaziU NATURAL JOIN Mjesto`, { type: QueryTypes.SELECT });

        // Get restrictions from database
        var ogranicenja = await sequelize.query(`SELECT * FROM Ogranicenje`, { type: QueryTypes.SELECT });

        // Display required page
        res.render('extras', {title: 'BITFIT - Ostalo', active: "Ostalo", adminflag: req.session.adminflag, greska: req.app.locals.greska, success: req.app.locals.success, lokacije: lokacije, ogranicenja: ogranicenja, zaposlenik: req.session.imeprezime});

         // Reset error & success messages after displaying
        req.app.locals.success = null;
        req.app.locals.greska = null;
     }
});


router.post('/deleteRestriction', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

    else{

        try{

            // Deleting restriction from database
	        await Ogranicenje.destroy({where: {idogranicenje: req.body.restrictionid}});

            // Set success message
            req.app.locals.success = `Ogranicenje (ID: ${req.body.restrictionid}) uspješno obrisano.`

        }catch(err){

            console.log(err);
            req.app.locals.greska = `Dogodila se greška.`
        }

         // Display required page
        res.redirect('/extras');
    }
});


// POST Request for adding restriction to DB
router.post('/addRestriction', async function (req, res, next) {

    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

    else{

        // Validation data
	    let data = {
            restriction_name:  req.body.restrictionname
        };

        // Validation
	    var status = restrictionSchema.validate(data, {abortEarly: false});

            // If validation is not OK
	        if(status.error)
		        req.app.locals.greska = status.error.details[0].message;
    
            // If validation is OK
            else{

                var ogranicenja = await sequelize.query(`SELECT nazivOgranicenje FROM Ogranicenje WHERE LOWER(nazivOgranicenje) = '${req.body.restrictionname.toLowerCase()}';`,  
                { type: QueryTypes.SELECT });


                if(ogranicenja.length == 0){


                try{

                // Inserting restriction
                await sequelize.query(`INSERT INTO Ogranicenje (nazivOgranicenje) VALUES ('${req.body.restrictionname}');`,  
                { type: QueryTypes.INSERT });

                // Set success message
                req.app.locals.success = `Ograničenje "${req.body.restrictionname}" uspješno dodano.`


                }catch(err){

                    console.log(err);
                    req.app.locals.greska = `Dogodila se greška.`
                }
                }
                else req.app.locals.greska = `Ovo ogranicenje vec postoji!`
            }

        // Display required page
        res.redirect('/extras');
    }
});


// GET* Request for info about location
router.post('/locationInfo', async function (req, res, next) {
 
    if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

    else{

        // Get location info
        var location = await sequelize.query(
        `SELECT * 
        FROM Lokacija NATURAL JOIN SeNalaziU
        NATURAL JOIN Mjesto
        WHERE idLokacija = ${req.body.idlokacija}`,
        { type: QueryTypes.SELECT });

        // Get last 10 terms on specified location
        var terms = await sequelize.query(
        `SELECT idTermin,
        to_char(vrijemePocetak::DATE, 'dd.mm.yyyy') AS datum,
        to_char(vrijemePocetak::time , 'HH24:MI') AS vrijemePocetak,
        to_char(vrijemeZavrsetak::time , 'HH24:MI') as vrijemeZavrsetak,
        maxBrojClanova, imeOsoba, prezimeOsoba, nazivLokacija, nazivIntenzitet, nazivVrsta
        FROM Termin NATURAL JOIN Voditelj NATURAL JOIN Osoba
        NATURAL JOIN Intenzitet NATURAL JOIN Vrsta NATURAL JOIN Lokacija
        WHERE idLokacija=${req.body.idlokacija} LIMIT 10`,
        { type: QueryTypes.SELECT });


        // Display required page
        res.render('locationInfo', {title: 'BITFIT - Ostalo', active: "Ostalo", adminflag: req.session.adminflag, greska: req.app.locals.greska, success: req.app.locals.success, lokacija: location[0], terms: terms, zaposlenik: req.session.imeprezime});
    
        // Reset error & success messages after displaying
        req.app.locals.success = null;
        req.app.locals.greska = null;
    }
});




module.exports = router;