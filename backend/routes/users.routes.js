const express = require('express');

const { QueryTypes } = require('sequelize');
const { Korisnik, Osoba, Clanarina, sequelize } = require('../extras/sequelize');

const Joi = require('@hapi/joi');

const router = express.Router();

const bcrypt = require('bcrypt');
const saltRounds = 10;

const functions = require('../extras/functions');
const logger = require('../extras/logger');
const settings = require('../extras/settings');

router.use(express.json());
router.use(express.urlencoded({extended: true}));

// VALIDATION Schema
const userSchema = Joi.object().keys({

    email: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).email().required()
	.messages(settings.VALIDATION.emailMessages),

    first_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.firstNameMessages),

	last_name: Joi.string().min(settings.RULES.minLength).max(settings.RULES.maxLength).required()
	.messages(settings.VALIDATION.lastNameMessages),

     oib: Joi.string().length(11).pattern(/^[0-9]+$/).required()
     .messages(settings.VALIDATION.oibMessages),

     password: Joi.string().min(settings.RULES.minPasswordLength).max(settings.RULES.maxPasswordLength).required()
     .messages(settings.VALIDATION.passwordMessages),

     date_of_birth: Joi.string().pattern(/^((0[1-9])|(1[\d])|(2[\d])|(3[01]))[.]((0[1-9])|(1[12]))[.]([\d]{4})[.]$/).required()
     .messages(settings.VALIDATION.dateOfBirthMessages)
});


// GET Request to display all users
router.get('/', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get all users & their membership expiration
          var korisnici = await sequelize.query(
          `SELECT Korisnik.idKorisnik, idOsoba, imeOsoba, prezimeOsoba, to_char(MAX(vrijemeIstekaClanarina)::DATE, 'dd.mm.yyyy.') as istekClanarina,
           to_char(datumRodenjaOsoba::DATE, 'dd.mm.yyyy.') as datumRodenjaOsoba, oibOsoba, emailOsoba 
	      FROM Korisnik NATURAL JOIN Osoba LEFT JOIN Clanarina on Korisnik.idKorisnik = Clanarina.idKorisnik
	      GROUP BY Korisnik.idKorisnik, imeOsoba, prezimeOsoba, datumRodenjaOsoba, oibOsoba, emailOsoba;`,
          { type: QueryTypes.SELECT });

          // Display required page
          res.render('users', {title: 'BITFIT - Korisnici', active: "Korisnici", adminflag: req.session.adminflag,  greska: req.app.locals.greska, korisnici: korisnici, success: req.app.locals.success, zaposlenik: req.session.imeprezime});

          // Reset error & success messages after displaying
          req.app.locals.success = null;
          req.app.locals.greska = null;
     }
});


router.post('/info', async function (req, res, next) {


     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Depending if we come to INFO from Modal or from Table function button
          if(!req.query.userid) req.query.userid = req.body.userid;
     
          // Get user info from DB
          var korisnik = await sequelize.query(`SELECT *, to_char(datumRodenjaOsoba::DATE, 'dd.mm.yyyy.') as datumRodenjaOsoba FROM Korisnik NATURAL JOIN Osoba WHERE idKorisnik = ${req.query.userid}`, { type: QueryTypes.SELECT });
          console.log(korisnik.length);

          // If there is user with specified ID
          if(korisnik.length != 0){
     

               // Get memberships info from DB
               var clanarine = await sequelize.query(`SELECT *, to_char(vrijemePlacanjaClanarina::DATE, 'dd.mm.yyyy.') AS vrijemePlacanjaClanarina,
               to_char(vrijemeIstekaClanarina::DATE, 'dd.mm.yyyy.') AS vrijemeIstekaClanarina FROM Clanarina WHERE idKorisnik = ${req.query.userid} ORDER BY idClanarina DESC`, {type: QueryTypes.SELECT});
               if(clanarine.length == 0) clanarine = null;

               // Get last 10 terms reserved by user
               var treninzi = await sequelize.query(`SELECT idTermin, nazivIntenzitet, nazivVrsta, to_char(vrijemePocetak::DATE, 'dd.mm.yyyy.') AS datum,
               to_char(vrijemeRezervacija::time , 'HH24:MI') as vrijemeRezervacija, statusDolaska , nazivLokacija 
               FROM Korisnik NATURAL JOIN Rezervira NATURAL JOIN Termin NATURAL JOIN Lokacija NATURAL JOIN Intenzitet NATURAL JOIN Vrsta
               WHERE idKorisnik = ${req.query.userid}
               LIMIT 10`, {type: QueryTypes.SELECT});
     

               // Display required page
               res.render('userInfo', {title: 'BITFIT - Detalji o korisniku', adminflag: req.session.adminflag,  active: "Korisnici", greska: req.app.locals.greska, korisnik: korisnik[0], clanarine: clanarine, success: req.app.locals.success, treninzi: treninzi, zaposlenik: req.session.imeprezime});
     
               // Clear msgs
               req.app.locals.success = null;
               req.app.locals.greska = null;
          } 

          else{

               // Set error message
               req.app.locals.greska = `Ne postoji korisnik sa ID: ${req.query.userid}`;
          
               // Redirect to all users page
               res.redirect('/users');
          }
     }
});


// POST request to remove user
router.post('/removeUser', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

	     // Deleting user & person from database
	     await Korisnik.destroy({where: {idkorisnik: req.body.userid}});
          await Osoba.destroy({where: {idosoba: req.body.personid}});

	     // Setting success message
	     req.app.locals.success = `Korisnik (ID: ${req.body.userid}) uspješno obrisan iz baze podataka!`

	     // Logging into actions.txt
	     logger.actions.info(`${functions.dateTime} - Removed user ${req.body.username}`);

	     // Render required page
          res.redirect('/users');
     }
});

// Post Request to insert user
router.post('/insertUser', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Validation DATA
	     let data = {

               password: req.body.password,
               email: req.body.email, 
               date_of_birth: req.body.date_of_birth, 
               oib: req.body.oib, 
               first_name: req.body.first_name, 
               last_name: req.body.last_name
          };

          // Validation
	     var status = userSchema.validate(data, {abortEarly: false});

          // If validation is not OK
	     if(status.error)
		     req.app.locals.greska = status.error.details[0].message;

          // If validation is OK
          else{

               // Check if EMAIL or OIB exists in database
               var persons = await sequelize.query(`SELECT * FROM Osoba WHERE oibOsoba = ${req.body.oib} OR emailOsoba = '${req.body.email}';`,
               { type: QueryTypes.SELECT });

               
               if(persons.length == 0){
	          try{

                    var hash = await bcrypt.hash(req.body.password, saltRounds);
    
                    
                    // Inserting person into database
                    await sequelize.query(
                    `INSERT INTO Osoba (emailOsoba, imeOsoba, prezimeOsoba, oibOsoba, lozinkaOsoba, datumRodenjaOsoba) VALUES
                    ('${req.body.email}','${req.body.first_name}','${req.body.last_name}','${req.body.oib}','${hash}','${req.body.date_of_birth}');`,
                    { type: QueryTypes.INSERT });


                    // Get ID from person (FK for User Column)
                    var query = await sequelize.query(`SELECT idOsoba FROM Osoba WHERE emailOsoba = '${req.body.email}'`, {type: QueryTypes.SELECT});
     
                    // Inserting user into database
                    await sequelize.query(
                    `INSERT INTO Korisnik (idosoba) VALUES (${query[0].idosoba});`,
                    { type: QueryTypes.INSERT });

                    // Set success message
                    req.app.locals.success = `Korisnik (${req.body.email}) uspješno upisan u bazu podataka.`

                    // Logging into actions.txt
	               logger.actions.info(`${functions.dateTime} - Added user ${req.body.email}`);
                    
               

               }catch(err){ 
                    console.log(err); 
                    req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`;
               }

               }

               else req.app.locals.greska = `Osoba sa ovim e-mailom ili OIB-om je već u bazi!`;

          
               // Render required page
               res.redirect('/users');
          }
     }
});

// Post request to extend monthly membership
router.post('/extendMonthlyMembership', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          // Get last membership
          var zadnjaClanarina = await sequelize.query(`SELECT vrijemeIstekaClanarina as istek FROM Clanarina WHERE idkorisnik = ${req.body.userid} ORDER BY idclanarina DESC LIMIT 1 
`         , {type: QueryTypes.SELECT});

          //console.log("Zadnja clanarina: " + zadnjaClanarina[0].istek);

          var date = new Date();
          var year = date.getFullYear();

          var month = date.getMonth()+1;
          // Fixing month format
          if(month < 10) month= "0"+month;

          var day = date.getDate();

          // Fixing day format
          if(day < 10) day="0"+day;

          // Forming new date
          var newdate =year + "-" + month + "-" + day;

          // If user has no active membership, extend 1 month from today's date
          if(zadnjaClanarina[0] == undefined || zadnjaClanarina[0].istek < newdate){

               await sequelize.query(
               `INSERT INTO Clanarina (idKorisnik, vrijemePlacanjaClanarina, vrijemeIstekaClanarina, cijenaClanarina, propusteniDolasci) VALUES
               (${req.body.userid}, date '${newdate}',date '${newdate}' + interval '1 month', '170', '0');`,
               { type: QueryTypes.INSERT });
          }

          // If user has an active membership, extend 1 month from expire date
          if(zadnjaClanarina[0] != undefined && zadnjaClanarina[0].istek >= newdate){

               await sequelize.query(
               `INSERT INTO Clanarina (idKorisnik, vrijemePlacanjaClanarina, vrijemeIstekaClanarina, cijenaClanarina, propusteniDolasci) VALUES
               (${req.body.userid}, date '${zadnjaClanarina[0].istek}', date '${zadnjaClanarina[0].istek}' + interval '1 month','170','0');`,
           { type: QueryTypes.INSERT });
          }

          // Set success message
          req.app.locals.success = `Članarina za korisnika (ID: ${req.body.userid}) uspješno produžena.`

          // Logging into actions.txt
	     logger.actions.info(`${functions.dateTime} - Extended membership for ID: ${req.body.username}`);

          // Render required page
          res.redirect(307, '/users/info?userid=' + req.body.userid);
     }
}),

// POST Request for removing monthly membership
router.post('/removeMonthlyMembership', async function (req, res, next) {

     if(!req.session.loggedin) res.render('login', settings.NOT_LOGGED_IN_PARAMS);

     else{

          try{
    
          // Delete from DB
          await Clanarina.destroy({where: { idclanarina: req.body.membershipid }});

          // Set success message
          req.app.locals.success = `Članarina za korisnika (ID: ${req.body.userid}) uspješno uklonjena.`

          // Logging into actions.txt
	     logger.actions.info(`${functions.dateTime} - Removed membership for ID: ${req.body.username}`);

          }catch(err){
               console.log(err);
               req.app.locals.greska = `Dogodila se greška. Molimo obratite se administratoru.`

          }

          // Render required page
          res.redirect(307, '/users/info?userid=' + req.body.userid);
     }
})

module.exports = router;