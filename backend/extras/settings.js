// A file for constants, database connections, etc.

// Application PORT
const PORT = 3000;

// DB Settings
const DB_CONFIG = {
    host: 'localhost',
    dialect: 'postgres',
    logging: false,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    } 
}

// Parametri za not logged in gresku
var NOT_LOGGED_IN_PARAMS = {

    title: `BITFIT - Prijava`,
    greska: `Niste prijavljeni u sustav!`,
    success: null,
    adminflag: null,
    active: null
}


// Pravila za validaciju
const RULES = {

    minLength: 3,
    maxLength: 30,
    minPasswordLength: 8,
    maxPasswordLength: 30,
    minRestrictionLength: 10,
    maxRestrictionLength: 50,
    minUsers: 1,
    maxUsers: 50
}

    var loginSuccessMsg = "Uspješno ste se prijavili u sustav";
    var loginErrorMsg = "Neispravan email ili lozinka!";
    var defaultErrorMsg = "Dogodila se greška.";
    var registrationSuccessMsg = "Registracija uspješna. Molimo prijavite se.";
    var userExistsMsg = "Korisnik je već u sustavu!";

// Validation settings
const VALIDATION = {

    oibMessages: {
        "string.empty": `OIB ne smije biti prazan!`,
        "string.length": `OIB mora imati 11 znamenki!`,
        "string.pattern": `OIB smije sadržavati samo brojeve!`,
        "any.required": `OIB je obavezan!`
    },

    emailMessages: {

        "string.empty": `E-Mail ne smije biti prazan!`,
        "string.min": `E-Mail ne smije biti kraći od ${RULES.minLength} znakova!`,
        "string.max": `E-Mail ne smije biti dulji od ${RULES.maxLength} znakova!`,
        "any.required": `E-Mail je obavezan!`
    },

    passwordMessages: {

        "string.empty": `Lozinka ne može biti prazna!`,
        "string.min": `Lozinka ne može biti kraća od ${RULES.minPasswordLength} znakova!`,
        "string.max": `Lozinka ne smije biti dulja od ${RULES.maxPasswordLength} znakova!`,
        "any.required": `Lozinka je obavezna!`
    },

    firstNameMessages: {

        "string.empty": `Ime ne smije biti prazno!`,
        "string.min": `Ime ne smije biti kraće od ${RULES.minLength} slova!`,
        "string.max": `Ime ne smije biti dulje od ${RULES.maxLength} slova!`,
        "any.required": `Ime je obavezno!`,
        "object.regex": `Ime smije sadržavati samo slova!`,
        "string.pattern.base": `Ime smije sadržavati samo slova!`
    },

    lastNameMessages: {

        "string.empty": `Prezime ne smije biti prazno!`,
        "string.min": `Prezime ne smije biti kraće od ${RULES.minLength} slova!`,
        "string.max": `Prezime ne smije biti dulje od ${RULES.maxLength} slova!`,
        "any.required": `Prezime je obavezno!`,
        "object.regex": `Prezime smije sadržavati samo slova!`,
        "string.pattern.base": `Prezime smije sadržavati samo slova!`
    },

    dateOfBirthMessages: {

        "string.empty": `Datum rođenja ne može biti prazan!`,
        "string.pattern": `Datum rođenja mora biti oblika "dd.mm.yyyy." !`,
        "any.required": `Datum rođenja je obavezan!`
    },
    
    restrictionMessages: {

        "string.empty": `Naziv ograničenja ne može biti prazan!`,
        "string.min": `Naziv ograničenja ne može imati manje od ${RULES.minRestrictionLength} znakova!`,
        "string.max": `Naziv ograničenja ne može imati više od ${RULES.maxRestrictionLength} znakova!`,
        "any.required": `Naziv ograničenja je obavezan!`,
    },


    coachMessages: {
        "number.empty": `Odabir voditelja je obavezan!`,
        "any.required": `Odabir voditelja je obavezan!`
    },

    locationMessages: {
        "number.empty": `Odabir lokacije je obavezan!`,
        "any.required": `Odabir lokacije je obavezan!`
    },

    maxUsersMessages: {
        "number.empty": `Maksimalni broj članova ne smije biti prazan!`,
        "number.min": `Maksimalni broj članova smije biti manji od od ${RULES.minUsers}!`,
        "number.max": `Maksimalni broj članova ne smije biti veći od ${RULES.maxUsers}!`,
        "any.required": `Maksimalni broj članova je obavezan!`
    },

    termStartMessages: {
        "string.empty": `Vrijeme početka ne smije biti prazno!`,
        "any.required": `Vrijeme početka je obavezno!`
    },

    termEndMessages: {
        "string.empty": `Vrijeme završetka ne smije biti prazno!`,
        "any.required": `Vrijeme završetka je obavezno!`
    },

    termDateMessages: {
        "string.empty": `Datum termina ne smije biti prazan!`,
        "any.required": `Datum termina je obavezan!`
    },
}

exports.NOT_LOGGED_IN_PARAMS = NOT_LOGGED_IN_PARAMS;
exports.RULES = RULES;
exports.VALIDATION = VALIDATION;
exports.PORT = PORT;
exports.DB_CONFIG = DB_CONFIG;

exports.LOGIN_SUCC_MSG = loginSuccessMsg;
exports.LOGIN_ERR_MSG = loginErrorMsg;
exports.DEFAULT_ERR_MSG = defaultErrorMsg;
exports.REG_SUCC_MSG = registrationSuccessMsg;
exports.URS_EXISTS_MSG = userExistsMsg;