const Sequelize = require('sequelize');

const settings = require('../extras/settings');
const sequelize = new Sequelize('BITFIT', 'postgres', 'bazepodataka', settings.DB_CONFIG);

const KorisnikModel = require('../models/korisnik');
const Korisnik = KorisnikModel(sequelize, Sequelize);

const OsobaModel = require('../models/osoba');
const Osoba = OsobaModel(sequelize, Sequelize);

const VoditeljModel = require('../models/voditelj');
const Voditelj = VoditeljModel(sequelize, Sequelize);

const ClanarinaModel = require('../models/clanarina');
const Clanarina = ClanarinaModel(sequelize, Sequelize);

const OgranicenjeModel = require('../models/ogranicenje');
const Ogranicenje = OgranicenjeModel(sequelize, Sequelize);

const ZaposlenikModel = require('../models/zaposlenik');
const Zaposlenik = ZaposlenikModel(sequelize, Sequelize);

Osoba.belongsTo(Korisnik);

Osoba.belongsTo(Zaposlenik);

sequelize.sync({ force: false });


module.exports = {
  Korisnik,
  Zaposlenik,
  Osoba,
  Voditelj,
  Clanarina,
  Ogranicenje,
  sequelize
}
