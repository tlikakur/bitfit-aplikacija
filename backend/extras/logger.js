const path = require('path');
const winston = require('winston');

const actions = winston.createLogger({

    level: 'info',
    format: winston.format.printf(info => `${info.message}`),
    transports: [
      new winston.transports.File({

        // File for actions (Manually written in code)
        filename: path.join(__dirname, '/logs/actions.log'),
        level: 'info',
        maxsize: 500
      })
    ],

    // File for exceptions
    exceptionHandlers: [
      new winston.transports.File({ filename: path.join(__dirname, '/logs/exceptions.log') })
    ]

    
  });


  module.exports = {
    actions : actions
  }
