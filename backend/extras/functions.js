// A function that returns current date and time
function dateTime(){

    let date = new Date(Date.now());
    return (date.getDate() + "." + Number(date.getMonth()+1) + "." + date.getFullYear() + " (" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ")");
}

exports.dateTime = dateTime;
