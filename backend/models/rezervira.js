const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rezervira', {
    vrijemerezervacija: {
      type: DataTypes.DATE,
      allowNull: false
    },
    statusdolaska: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    idtermin: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'termin',
        key: 'idtermin'
      }
    },
    idkorisnik: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'korisnik',
        key: 'idkorisnik'
      }
    }
  }, {
    sequelize,
    tableName: 'rezervira',
    schema: 'public',
    hasTrigger: true,
    timestamps: false,
    indexes: [
      {
        name: "rezervira_pkey",
        unique: true,
        fields: [
          { name: "idtermin" },
          { name: "idkorisnik" },
        ]
      },
    ]
  });
};
