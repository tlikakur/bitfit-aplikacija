const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ogranicenje', {
    idogranicenje: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivogranicenje: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "ogranicenje_nazivogranicenje_key"
    }
  }, {
    sequelize,
    tableName: 'ogranicenje',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "ogranicenje_nazivogranicenje_key",
        unique: true,
        fields: [
          { name: "nazivogranicenje" },
        ]
      },
      {
        name: "ogranicenje_pkey",
        unique: true,
        fields: [
          { name: "idogranicenje" },
        ]
      },
    ]
  });
};
