const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('zavrsio', {
    datumzavrsetka: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    idprogram: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'programiosposobljavanja',
        key: 'idprogram'
      }
    },
    idvoditelj: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'voditelj',
        key: 'idvoditelj'
      }
    }
  }, {
    sequelize,
    tableName: 'zavrsio',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "zavrsio_pkey",
        unique: true,
        fields: [
          { name: "idprogram" },
          { name: "idvoditelj" },
        ]
      },
    ]
  });
};
