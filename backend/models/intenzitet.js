const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('intenzitet', {
    idintenzitet: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivintenzitet: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "intenzitet_nazivintenzitet_key"
    },
    opisintenzitet: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'intenzitet',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "intenzitet_nazivintenzitet_key",
        unique: true,
        fields: [
          { name: "nazivintenzitet" },
        ]
      },
      {
        name: "intenzitet_pkey",
        unique: true,
        fields: [
          { name: "idintenzitet" },
        ]
      },
    ]
  });
};
