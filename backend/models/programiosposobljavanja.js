const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('programiosposobljavanja', {
    idprogram: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivprogram: {
      type: DataTypes.STRING(40),
      allowNull: false,
      unique: "programiosposobljavanja_nazivprogram_key"
    }
  }, {
    sequelize,
    tableName: 'programiosposobljavanja',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "programiosposobljavanja_nazivprogram_key",
        unique: true,
        fields: [
          { name: "nazivprogram" },
        ]
      },
      {
        name: "programiosposobljavanja_pkey",
        unique: true,
        fields: [
          { name: "idprogram" },
        ]
      },
    ]
  });
};
