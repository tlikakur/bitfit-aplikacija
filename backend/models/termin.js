const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('termin', {
    idtermin: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    vrijemepocetak: {
      type: DataTypes.DATE,
      allowNull: false
    },
    vrijemezavrsetak: {
      type: DataTypes.DATE,
      allowNull: false
    },
    maxbrojclanova: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    idvoditelj: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'voditelj',
        key: 'idvoditelj'
      }
    },
    idlokacija: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'lokacija',
        key: 'idlokacija'
      }
    },
    idintenzitet: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'intenzitet',
        key: 'idintenzitet'
      }
    },
    idvrsta: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'vrsta',
        key: 'idvrsta'
      }
    }
  }, {
    sequelize,
    tableName: 'termin',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "termin_pkey",
        unique: true,
        fields: [
          { name: "idtermin" },
        ]
      },
    ]
  });
};
