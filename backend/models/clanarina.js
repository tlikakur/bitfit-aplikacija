const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('clanarina', {
    idclanarina: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    vrijemeplacanjaclanarina: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    propustenidolasci: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cijenaclanarina: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    vrijemeistekaclanarina: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    idkorisnik: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'korisnik',
        key: 'idkorisnik'
      }
    }
  }, {
    sequelize,
    tableName: 'clanarina',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "clanarina_pkey",
        unique: true,
        fields: [
          { name: "idclanarina" },
        ]
      },
    ]
  });
};
