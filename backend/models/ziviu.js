const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ziviu', {
    adresazaposlenik: {
      type: DataTypes.STRING(40),
      allowNull: false
    },
    idzaposlenik: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'zaposlenik',
        key: 'idzaposlenik'
      }
    },
    pbmjesto: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'mjesto',
        key: 'pbmjesto'
      }
    }
  }, {
    sequelize,
    tableName: 'ziviu',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "ziviu_pkey",
        unique: true,
        fields: [
          { name: "idzaposlenik" },
        ]
      },
    ]
  });
};
