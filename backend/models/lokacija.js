const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lokacija', {
    idlokacija: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivlokacija: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "lokacija_nazivlokacija_key"
    },
    vanjskalokacija: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'lokacija',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "lokacija_nazivlokacija_key",
        unique: true,
        fields: [
          { name: "nazivlokacija" },
        ]
      },
      {
        name: "lokacija_pkey",
        unique: true,
        fields: [
          { name: "idlokacija" },
        ]
      },
    ]
  });
};
