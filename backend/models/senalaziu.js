const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('senalaziu', {
    adresalokacija: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    idlokacija: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'lokacija',
        key: 'idlokacija'
      }
    },
    pbmjesto: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'mjesto',
        key: 'pbmjesto'
      }
    }
  }, {
    sequelize,
    tableName: 'senalaziu',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "senalaziu_pkey",
        unique: true,
        fields: [
          { name: "idlokacija" },
        ]
      },
    ]
  });
};
