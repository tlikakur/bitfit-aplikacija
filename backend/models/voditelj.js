const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('voditelj', {
    idvoditelj: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    zavrsenkif: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    idosoba: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'osoba',
        key: 'idosoba'
      },
      unique: "voditelj_idosoba_key"
    }
  }, {
    sequelize,
    tableName: 'voditelj',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "voditelj_idosoba_key",
        unique: true,
        fields: [
          { name: "idosoba" },
        ]
      },
      {
        name: "voditelj_pkey",
        unique: true,
        fields: [
          { name: "idvoditelj" },
        ]
      },
    ]
  });
};
