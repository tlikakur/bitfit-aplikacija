const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ogranicenjazatrening', {
    idtermin: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'termin',
        key: 'idtermin'
      }
    },
    idogranicenje: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'ogranicenje',
        key: 'idogranicenje'
      }
    }
  }, {
    sequelize,
    tableName: 'ogranicenjazatrening',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "ogranicenjazatrening_pkey",
        unique: true,
        fields: [
          { name: "idtermin" },
          { name: "idogranicenje" },
        ]
      },
    ]
  });
};
