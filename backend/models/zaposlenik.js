const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('zaposlenik', {
    idzaposlenik: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    jeadmin: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    idosoba: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'osoba',
        key: 'idosoba'
      },
      unique: "zaposlenik_idosoba_key"
    }
  }, {
    sequelize,
    tableName: 'zaposlenik',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "zaposlenik_idosoba_key",
        unique: true,
        fields: [
          { name: "idosoba" },
        ]
      },
      {
        name: "zaposlenik_pkey",
        unique: true,
        fields: [
          { name: "idzaposlenik" },
        ]
      },
    ]
  });
};
