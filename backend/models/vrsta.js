const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('vrsta', {
    idvrsta: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivvrsta: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "vrsta_nazivvrsta_key"
    },
    opisvrsta: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'vrsta',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "vrsta_nazivvrsta_key",
        unique: true,
        fields: [
          { name: "nazivvrsta" },
        ]
      },
      {
        name: "vrsta_pkey",
        unique: true,
        fields: [
          { name: "idvrsta" },
        ]
      },
    ]
  });
};
