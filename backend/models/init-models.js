var DataTypes = require("sequelize").DataTypes;
var _clanarina = require("./clanarina");
var _intenzitet = require("./intenzitet");
var _korisnik = require("./korisnik");
var _lokacija = require("./lokacija");
var _mjesto = require("./mjesto");
var _ogranicenjazatrening = require("./ogranicenjazatrening");
var _ogranicenje = require("./ogranicenje");
var _osoba = require("./osoba");
var _programiosposobljavanja = require("./programiosposobljavanja");
var _rezervira = require("./rezervira");
var _senalaziu = require("./senalaziu");
var _termin = require("./termin");
var _voditelj = require("./voditelj");
var _vrsta = require("./vrsta");
var _zaposlenik = require("./zaposlenik");
var _zavrsio = require("./zavrsio");
var _ziviu = require("./ziviu");

function initModels(sequelize) {
  var clanarina = _clanarina(sequelize, DataTypes);
  var intenzitet = _intenzitet(sequelize, DataTypes);
  var korisnik = _korisnik(sequelize, DataTypes);
  var lokacija = _lokacija(sequelize, DataTypes);
  var mjesto = _mjesto(sequelize, DataTypes);
  var ogranicenjazatrening = _ogranicenjazatrening(sequelize, DataTypes);
  var ogranicenje = _ogranicenje(sequelize, DataTypes);
  var osoba = _osoba(sequelize, DataTypes);
  var programiosposobljavanja = _programiosposobljavanja(sequelize, DataTypes);
  var rezervira = _rezervira(sequelize, DataTypes);
  var senalaziu = _senalaziu(sequelize, DataTypes);
  var termin = _termin(sequelize, DataTypes);
  var voditelj = _voditelj(sequelize, DataTypes);
  var vrsta = _vrsta(sequelize, DataTypes);
  var zaposlenik = _zaposlenik(sequelize, DataTypes);
  var zavrsio = _zavrsio(sequelize, DataTypes);
  var ziviu = _ziviu(sequelize, DataTypes);

  korisnik.belongsToMany(termin, { through: rezervira, foreignKey: "idkorisnik", otherKey: "idtermin" });
  ogranicenje.belongsToMany(termin, { through: ogranicenjazatrening, foreignKey: "idogranicenje", otherKey: "idtermin" });
  programiosposobljavanja.belongsToMany(voditelj, { through: zavrsio, foreignKey: "idprogram", otherKey: "idvoditelj" });
  termin.belongsToMany(korisnik, { through: rezervira, foreignKey: "idtermin", otherKey: "idkorisnik" });
  termin.belongsToMany(ogranicenje, { through: ogranicenjazatrening, foreignKey: "idtermin", otherKey: "idogranicenje" });
  voditelj.belongsToMany(programiosposobljavanja, { through: zavrsio, foreignKey: "idvoditelj", otherKey: "idprogram" });
  termin.belongsTo(intenzitet, { as: "idintenzitet_intenzitet", foreignKey: "idintenzitet"});
  intenzitet.hasMany(termin, { as: "termins", foreignKey: "idintenzitet"});
  clanarina.belongsTo(korisnik, { as: "idkorisnik_korisnik", foreignKey: "idkorisnik"});
  korisnik.hasMany(clanarina, { as: "clanarinas", foreignKey: "idkorisnik"});
  rezervira.belongsTo(korisnik, { as: "idkorisnik_korisnik", foreignKey: "idkorisnik"});
  korisnik.hasMany(rezervira, { as: "rezerviras", foreignKey: "idkorisnik"});
  senalaziu.belongsTo(lokacija, { as: "idlokacija_lokacija", foreignKey: "idlokacija"});
  lokacija.hasOne(senalaziu, { as: "senalaziu", foreignKey: "idlokacija"});
  termin.belongsTo(lokacija, { as: "idlokacija_lokacija", foreignKey: "idlokacija"});
  lokacija.hasMany(termin, { as: "termins", foreignKey: "idlokacija"});
  senalaziu.belongsTo(mjesto, { as: "pbmjesto_mjesto", foreignKey: "pbmjesto"});
  mjesto.hasMany(senalaziu, { as: "senalazius", foreignKey: "pbmjesto"});
  ziviu.belongsTo(mjesto, { as: "pbmjesto_mjesto", foreignKey: "pbmjesto"});
  mjesto.hasMany(ziviu, { as: "zivius", foreignKey: "pbmjesto"});
  ogranicenjazatrening.belongsTo(ogranicenje, { as: "idogranicenje_ogranicenje", foreignKey: "idogranicenje"});
  ogranicenje.hasMany(ogranicenjazatrening, { as: "ogranicenjazatrenings", foreignKey: "idogranicenje"});
  korisnik.belongsTo(osoba, { as: "idosoba_osoba", foreignKey: "idosoba"});
  osoba.hasOne(korisnik, { as: "korisnik", foreignKey: "idosoba"});
  voditelj.belongsTo(osoba, { as: "idosoba_osoba", foreignKey: "idosoba"});
  osoba.hasOne(voditelj, { as: "voditelj", foreignKey: "idosoba"});
  zaposlenik.belongsTo(osoba, { as: "idosoba_osoba", foreignKey: "idosoba"});
  osoba.hasOne(zaposlenik, { as: "zaposlenik", foreignKey: "idosoba"});
  zavrsio.belongsTo(programiosposobljavanja, { as: "idprogram_programiosposobljavanja", foreignKey: "idprogram"});
  programiosposobljavanja.hasMany(zavrsio, { as: "zavrsios", foreignKey: "idprogram"});
  ogranicenjazatrening.belongsTo(termin, { as: "idtermin_termin", foreignKey: "idtermin"});
  termin.hasMany(ogranicenjazatrening, { as: "ogranicenjazatrenings", foreignKey: "idtermin"});
  rezervira.belongsTo(termin, { as: "idtermin_termin", foreignKey: "idtermin"});
  termin.hasMany(rezervira, { as: "rezerviras", foreignKey: "idtermin"});
  termin.belongsTo(voditelj, { as: "idvoditelj_voditelj", foreignKey: "idvoditelj"});
  voditelj.hasMany(termin, { as: "termins", foreignKey: "idvoditelj"});
  zavrsio.belongsTo(voditelj, { as: "idvoditelj_voditelj", foreignKey: "idvoditelj"});
  voditelj.hasMany(zavrsio, { as: "zavrsios", foreignKey: "idvoditelj"});
  termin.belongsTo(vrsta, { as: "idvrsta_vrstum", foreignKey: "idvrsta"});
  vrsta.hasMany(termin, { as: "termins", foreignKey: "idvrsta"});
  ziviu.belongsTo(zaposlenik, { as: "idzaposlenik_zaposlenik", foreignKey: "idzaposlenik"});
  zaposlenik.hasOne(ziviu, { as: "ziviu", foreignKey: "idzaposlenik"});

  return {
    clanarina,
    intenzitet,
    korisnik,
    lokacija,
    mjesto,
    ogranicenjazatrening,
    ogranicenje,
    osoba,
    programiosposobljavanja,
    rezervira,
    senalaziu,
    termin,
    voditelj,
    vrsta,
    zaposlenik,
    zavrsio,
    ziviu,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
