const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('osoba', {
    idosoba: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    imeosoba: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    prezimeosoba: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    datumrodenjaosoba: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    oibosoba: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      unique: "osoba_oibosoba_key"
    },
    emailosoba: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "osoba_emailosoba_key"
    },
    lozinkaosoba: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'osoba',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "osoba_emailosoba_key",
        unique: true,
        fields: [
          { name: "emailosoba" },
        ]
      },
      {
        name: "osoba_oibosoba_key",
        unique: true,
        fields: [
          { name: "oibosoba" },
        ]
      },
      {
        name: "osoba_pkey",
        unique: true,
        fields: [
          { name: "idosoba" },
        ]
      },
    ]
  });
};
