const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('korisnik', {
    idkorisnik: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    visinakorisnik: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tezinakorisnik: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    idosoba: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'osoba',
        key: 'idosoba'
      },
      unique: "korisnik_idosoba_key"
    }
  }, {
    sequelize,
    tableName: 'korisnik',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "korisnik_idosoba_key",
        unique: true,
        fields: [
          { name: "idosoba" },
        ]
      },
      {
        name: "korisnik_pkey",
        unique: true,
        fields: [
          { name: "idkorisnik" },
        ]
      },
    ]
  });
};
