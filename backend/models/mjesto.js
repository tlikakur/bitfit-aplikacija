const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mjesto', {
    pbmjesto: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nazivmjesto: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'mjesto',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "mjesto_pkey",
        unique: true,
        fields: [
          { name: "pbmjesto" },
        ]
      },
    ]
  });
};
